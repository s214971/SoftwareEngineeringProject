package presentation;

import java.util.*;

/**
 * Written by Andreas.
 */
public class Tokenizer {

	private String[] tokens;
	private int index;
	
	public Tokenizer(String input) throws ParsingException {
		tokenize(input);
		index = -1;
	}
	
	private void tokenize(String input) throws ParsingException {
		List<String> tokenList = new ArrayList<>();
		StringBuffer token = new StringBuffer();
		boolean lastIsWhitespace = true;
		boolean isInQuotes = false;
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c == '\"' || c == '\'') {
				isInQuotes = !isInQuotes;
			} else {
				if (Character.isWhitespace(c) && !isInQuotes) {
					if (!lastIsWhitespace)
						tokenList.add(token.toString());
					lastIsWhitespace = true;
					token.delete(0, token.length());
				}
				else {
					token.append(c);
					lastIsWhitespace = false;
				}
			}
		}
		if (!lastIsWhitespace)
			tokenList.add(token.toString());
		if (isInQuotes)
			throw new ParsingException("Quotes have no closure.");
		tokens = new String[tokenList.size()];
		tokenList.toArray(tokens);
	}

	public void advance() {
		if (!hasNext())
			throw new IllegalStateException("Reached end of token stream");
		index++;
	}
	
	public boolean hasNext() {
		return index + 1 < tokens.length;
	}
	
	public String current() {
		if (index < 0)
			throw new IllegalStateException("Advancing hasn't started.");
		return tokens[index];
	}
}
