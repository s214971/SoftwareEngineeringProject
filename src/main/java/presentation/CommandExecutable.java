package presentation;

/**
 * Written by Andreas
 */
public interface CommandExecutable {
	void run(CommandEngine engine) throws Exception;
}
