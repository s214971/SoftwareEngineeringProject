package presentation;
import java.util.Scanner;

import business.PlanningAppException;
import business.ProjectManager;
import business.TimeServer;

/**
 * Written by Andreas.
 * 
 * Entry-point of the program.
 */
public class Program {
	
    public static void main(String[] args) {
    	printStartingText();
    	startInterpretting();
    }
    
    private static void printStartingText() {
    	System.out.println("Planning app starting...");
    	System.out.println("Type commands to interact with the program. Command arguments are "
    			+ "seperated by an arbitrary amount of whitespace. Commands ignore letter case. Text wrapped in quotes"
    			+ " will result in the literal argument being used (useful for naming). "
    			+ "Project names may not start with a digit. Projects can be referred to by name "
    			+ "or ID (the system will know).");
    	System.out.println("Write '" + CommandEngine.HELP_COMMAND + "' to get the list of commands.");
    	System.out.println("Write '" + CommandEngine.EXIT_COMMAND + "' to terminate the program.");
    }
    
    private static void startInterpretting() {
    	Scanner scanner = new Scanner(System.in);
    	Interactor interactor = new Interactor(new ProjectManager());
    	while (true) {
    		try {
    			Tokenizer tokenizer = new Tokenizer(scanner.nextLine());
    			CommandEngine engine = new CommandEngine(interactor, tokenizer);
    			engine.interpretNext();
    		}
    		catch(ExitProgramException ex) {
    			break;
    		}
    		catch(ParsingException ex) {
    			System.out.println("Command couldn't be parsed: " + ex.getMessage());
    		}
    		catch (PlanningAppException ex) {
    			System.out.println("Invalid: " + ex.getMessage());
    		}
     		catch(Exception ex) {
    			System.out.println("Error encountered:\n" + ex.getMessage() + "\nContinue execution? (y/n)");
    			if (!readYOrN(scanner))
    				break;
    		}
    	}
    	System.out.println("Program terminated.");
    	scanner.close();
    }

    /**
     * Tries to read the next line as a y or n (case ignored). If none of those is given, it loops.
     * Returns true if y and false if n.
     */
	private static boolean readYOrN(Scanner scanner) {
		do {
			String line = scanner.nextLine();
			if (line.equalsIgnoreCase("y"))
				return true;
			else if (line.equalsIgnoreCase("n"))
				return false;
		} while (true);
	}
}
