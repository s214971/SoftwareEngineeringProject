package presentation;
import java.util.*;

import javax.naming.InvalidNameException;

import business.*;

public class CommandEngine {

	public static final String EXIT_COMMAND = "exit";
	public static final String HELP_COMMAND = "help";
	private static final String ADD_PROJECT_COMMAND = "AddProject";
	private static final String CREATE_DEVELOPER_COMMAND = "CreateDev";
	private static final String ADD_ACTIVITY_COMMAND = "AddAct";
	private static final String ASSIGN_DEV_TO_ACTIVITY_COMMAND = "AssignDevToAct";
	private static final String PRINT_ALL_COMMAND = "PrintAll";
	private static final String PRINT_PROJECT_COMMAND = "PrintProject";
	private static final String PRINT_DEVELOPERS_COMMAND = "PrintDevs";
	private static final String SET_ACTIVITY_TIMEFRAME_COMMAND = "SetActTimeFrame";
	private static final String REGISTER_DEVELOPER_HOURS_COMMAND = "RegisterDevHours";
	private static final String REMOVE_PROJECT_COMMAND = "RemoveProject";
	private static final String REMOVE_ACTIVITY_COMMAND = "RemoveAct";
	private static final String PRINT_DEV_PROJECTS_COMMAND = "PrintDevProjects";
	private static final String ASSIGN_FIXED_ACTIVITY_COMMAND = "AddFixedAct";
	private static final String REMOVE_FIXED_ACTIVITY_COMMAND = "RemoveFixedAct";
	private static final String RESET_REGISTERED_DEV_HOURS_COMMAND = "ResetDevHours";
	private static final String SET_EXPECTED_TIME_COMMAND = "SetExpectedTime";
	private static final String ADD_PROJECT_LEADER_COMMAND = "AddProjectLeader";
	private static final String REMOVE_PROJECT_LEADER_COMMAND = "RemoveProjectLeader";
	private static final String EDIT_PROJECT_NAME_COMMAND = "EditProjectName";
	private static final String EDIT_ACTIVITY_NAME_COMMAND = "EditActName";

	private Tokenizer tokenizer;
	private Interactor interactor;

	private static Map<String, CommandInfo> commandFunctionMap;

	//Written by Andreas
	public CommandEngine(Interactor interactor, Tokenizer tokenizer) {
		this.tokenizer = tokenizer;
		this.interactor = interactor;
		if (!commandsHaveInited())
			initCommands();
	}

	//Written by Andreas
	private boolean commandsHaveInited() {
		return commandFunctionMap != null;
	}

	//Written by Andreas
	private void initCommands() {
		commandFunctionMap = new HashMap<>();

		commandFunctionMap.put(HELP_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsHelp,
						HELP_COMMAND));

		commandFunctionMap.put(EXIT_COMMAND.toLowerCase(),
				new CommandInfo((e) -> { throw new ExitProgramException(); },
						EXIT_COMMAND));

		commandFunctionMap.put(ADD_PROJECT_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsAddProject,
						ADD_PROJECT_COMMAND + " <project>"));

		//Author: Mia
		commandFunctionMap.put(REMOVE_PROJECT_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsRemoveProject,
						REMOVE_PROJECT_COMMAND + " <project>"));
		
		//Author: Mia
		commandFunctionMap.put(EDIT_PROJECT_NAME_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsEditProjectName,
						EDIT_PROJECT_NAME_COMMAND + " <project> <new project name>"));

		commandFunctionMap.put(CREATE_DEVELOPER_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsCreateDeveloper,
						CREATE_DEVELOPER_COMMAND + " <developer name> <developer initials>"));

		commandFunctionMap.put(ADD_ACTIVITY_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsAddActivity,
						ADD_ACTIVITY_COMMAND + " <project> <activity name>"));
		
		//Author: Mia
		commandFunctionMap.put(REMOVE_ACTIVITY_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsRemoveActivity,
						REMOVE_ACTIVITY_COMMAND + " <project> <activity name>"));
		
		//Author: Mia
		commandFunctionMap.put(EDIT_ACTIVITY_NAME_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsEditActivityName,
						EDIT_ACTIVITY_NAME_COMMAND + " <project> <activity name> <new activity name>"));

		commandFunctionMap.put(ASSIGN_DEV_TO_ACTIVITY_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsAssignDevToActivity,
						ASSIGN_DEV_TO_ACTIVITY_COMMAND + " <developer initials> <containing project> <activity>"));

		commandFunctionMap.put(PRINT_ALL_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsPrintProjects,
						PRINT_ALL_COMMAND));
		
		commandFunctionMap.put(PRINT_PROJECT_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsPrintProject,
						PRINT_PROJECT_COMMAND + " <project>"));
		
		commandFunctionMap.put(PRINT_DEVELOPERS_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsPrintDevelopers,
						PRINT_DEVELOPERS_COMMAND));

		commandFunctionMap.put(SET_ACTIVITY_TIMEFRAME_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsSetActivityTimeFrame,
						SET_ACTIVITY_TIMEFRAME_COMMAND +" <project> <activity name> <start week> <start year> <end week> <end year>"));

		commandFunctionMap.put(REGISTER_DEVELOPER_HOURS_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsRegisterDeveloperHours,
						REGISTER_DEVELOPER_HOURS_COMMAND + " <developer initials> <containing project> <activity> <hours> <minutes>"));

		commandFunctionMap.put(PRINT_DEV_PROJECTS_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsPrintDevProjects,
						PRINT_DEV_PROJECTS_COMMAND + " <developer initials> "));

		commandFunctionMap.put(RESET_REGISTERED_DEV_HOURS_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsResetDevHours,
						RESET_REGISTERED_DEV_HOURS_COMMAND +" <project> <activity name> <developer id>"));

		commandFunctionMap.put(ASSIGN_FIXED_ACTIVITY_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsAssignFixedActivity,
						ASSIGN_FIXED_ACTIVITY_COMMAND + " <developer initials> <fixed activity name>"));

		commandFunctionMap.put(REMOVE_FIXED_ACTIVITY_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsRemoveFromFixedActivity,
						REMOVE_FIXED_ACTIVITY_COMMAND + " <developer initials> <fixed activity name>"));
		
		commandFunctionMap.put(SET_EXPECTED_TIME_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsSetExpectedTime,
						SET_EXPECTED_TIME_COMMAND + " <project> <activity name> <hours> <minutes>"));
		
		commandFunctionMap.put(ADD_PROJECT_LEADER_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsAddProjectLeader,
						ADD_PROJECT_LEADER_COMMAND + " <project> <developer id>"));
		
		commandFunctionMap.put(REMOVE_PROJECT_LEADER_COMMAND.toLowerCase(),
				new CommandInfo(CommandEngine::parseNextAsRemoveProjectLeader,
						REMOVE_PROJECT_LEADER_COMMAND + " <project>"));
	}

	//Written by Andreas
	private static String getSignature(String command) {
		return commandFunctionMap.get(command.toLowerCase()).getSignature();
	}

	//Written by Andreas
	public void interpretNext() throws Exception {
		if (!tokenizer.hasNext())
			return;
		tokenizer.advance();
		String token = tokenizer.current();
		if (commandFunctionMap.containsKey(token.toLowerCase()))
			commandFunctionMap.get(token.toLowerCase()).run(this);
		else
			throw new ParsingException("Unknown command. Write '" + HELP_COMMAND + "' for a list of commands.");
	}

	//Written by Andreas
	private static void parseNextAsAddProjectLeader(CommandEngine engine) throws ParsingException, NoElementException, ProjectLeaderException {
		if(!engine.tokenizer.hasNext()) 
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(ADD_PROJECT_LEADER_COMMAND) + "'.");
		engine.tokenizer.advance();
		String project = engine.tokenizer.current();
		
		if(!engine.tokenizer.hasNext())
			throw new ParsingException("An developer needs to be specified. Write '" + getSignature(ADD_PROJECT_LEADER_COMMAND) + "'.");
		engine.tokenizer.advance();
		String dev = engine.tokenizer.current();
		
		engine.interactor.addProjectLeader(project, dev);
	}
	
	//Written by Andreas
	private static void parseNextAsRemoveProjectLeader(CommandEngine engine) throws ParsingException, NoElementException, ProjectLeaderException {
		if(!engine.tokenizer.hasNext()) 
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(REMOVE_PROJECT_LEADER_COMMAND) + "'.");
		engine.tokenizer.advance();
		
		engine.interactor.removeProjectLeader(engine.tokenizer.current());
	}
	
	//Written by Andreas
	private static void parseNextAsSetExpectedTime(CommandEngine engine) throws ParsingException, NoElementException, InvalidTimeException {
		if(!engine.tokenizer.hasNext()) 
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(SET_EXPECTED_TIME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String project = engine.tokenizer.current();
		
		if(!engine.tokenizer.hasNext())
			throw new ParsingException("An activity needs to be specified. Write '" + getSignature(SET_EXPECTED_TIME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String activityName = engine.tokenizer.current();
		
		if(!engine.tokenizer.hasNext())
			throw new ParsingException("Hour count needs to be specified. Write '" + getSignature(SET_EXPECTED_TIME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String hourStr = engine.tokenizer.current();
		
		if(!engine.tokenizer.hasNext())
			throw new ParsingException("Minute count needs to be specified. Write '" + getSignature(SET_EXPECTED_TIME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String minuteStr = engine.tokenizer.current();
		
		long hours, minutes;
		try {
			hours = Long.valueOf(hourStr);
			minutes = Long.valueOf(minuteStr);
		} catch (NumberFormatException ex) {
			throw new ParsingException("The time cound not be converted to a number.");
		}
		
		engine.interactor.setExpectedTime(project, activityName, hours, minutes);
	}
	
	//Written by Nikolaj
	private static void parseNextAsResetDevHours(CommandEngine engine) throws ParsingException, NoElementException {
		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("A Project needs to be specified. Write '" + getSignature(RESET_REGISTERED_DEV_HOURS_COMMAND) + "'.");

		}
		engine.tokenizer.advance();
		String projectName = engine.tokenizer.current();

		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("An activity needs to be specified. Write '" + getSignature(RESET_REGISTERED_DEV_HOURS_COMMAND) + "'.");

		}
		engine.tokenizer.advance();
		String activityName = engine.tokenizer.current();

		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("A developers initials need to be specified. Write '" + getSignature(RESET_REGISTERED_DEV_HOURS_COMMAND) + "'.");

		}
		engine.tokenizer.advance();
		String devId = engine.tokenizer.current();

		engine.interactor.resetRegisteredDevHours(projectName, activityName, devId);



	}
	
	//Written by Nikolaj
	private static void parseNextAsPrintProjects(CommandEngine engine) throws InvalidTimeException {
		engine.interactor.printProjects();
	}
	
	//Author: Helena
	private static void parseNextAsPrintProject(CommandEngine engine) throws InvalidTimeException, NoElementException, ParsingException {
		if (!engine.tokenizer.hasNext()) {
			throw new ParsingException("Project needs to be specified. Write '" +getSignature(PRINT_PROJECT_COMMAND)+"'.");
		}
		engine.tokenizer.advance();
		engine.interactor.printProject(engine.tokenizer.current());
	}

	private static void parseNextAsPrintDevProjects(CommandEngine engine) throws InvalidTimeException, ParsingException{
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A developers initials need to be specified. Write '" + getSignature(PRINT_DEV_PROJECTS_COMMAND) + "'.");
		engine.tokenizer.advance();
		engine.interactor.printProjectsFromDev(engine.tokenizer.current());
	}
	
	// Author: Helena
	private static void parseNextAsPrintDevelopers(CommandEngine engine) {
		engine.interactor.printDevelopers();
	}

	//Written by Andreas
	private static void parseNextAsAddProject(CommandEngine engine) throws Exception {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Project needs a name. Write '" + getSignature(ADD_PROJECT_COMMAND) + "'.");
		engine.tokenizer.advance();
		engine.interactor.addProject(engine.tokenizer.current());
	}

	//Author: Mia
	private static void parseNextAsRemoveProject(CommandEngine engine) throws Exception {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Need to know which project to delete. Write '" + getSignature(REMOVE_PROJECT_COMMAND) + "'.");
		engine.tokenizer.advance();
		engine.interactor.removeProject(engine.tokenizer.current());
	}
	
	//Author: Mia
	private static void parseNextAsEditProjectName(CommandEngine engine) throws Exception {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Need to know which project to edit. Write '" + getSignature(EDIT_PROJECT_NAME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String oldProject = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Need to know new project name. Write '" + getSignature(EDIT_PROJECT_NAME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String newName = engine.tokenizer.current();
		engine.interactor.editProjectName(oldProject,newName);
	}

	//Written by Andreas
	private static void parseNextAsCreateDeveloper(CommandEngine engine) throws ParsingException, DuplicateException, IllegalNameException {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Developer needs a name. Write '" + getSignature(CREATE_DEVELOPER_COMMAND) + "'.");
		engine.tokenizer.advance();
		String name = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Developer needs initials. Write '" + getSignature(CREATE_DEVELOPER_COMMAND) + "'.");
		engine.tokenizer.advance();
		String initials = engine.tokenizer.current();
		engine.interactor.addDeveloper(name, initials);
	}

	//Written by Andreas
	private static void parseNextAsAddActivity(CommandEngine engine) throws ParsingException, DuplicateException, NoElementException, InvalidTimeException, IllegalNameException {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(ADD_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		String proj = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Activity needs a name. Write '" + getSignature(ADD_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		engine.interactor.addActivity(proj, engine.tokenizer.current());
	}

	//Author: Mia
	private static void parseNextAsRemoveActivity(CommandEngine engine) throws ParsingException, DuplicateException, NoElementException {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(REMOVE_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		String proj = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Need to know which activity to delete. Write '" + getSignature(REMOVE_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		engine.interactor.removeActivity(proj, engine.tokenizer.current());
	}
	
	//Author: Mia
	private static void parseNextAsEditActivityName(CommandEngine engine) throws ParsingException, NoElementException, IllegalNameException, DuplicateException {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(EDIT_ACTIVITY_NAME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String proj = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Need to know which activity to change. Write '" + getSignature(EDIT_ACTIVITY_NAME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String oldActivity = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Need to know new activity name. Write '" + getSignature(EDIT_ACTIVITY_NAME_COMMAND) + "'.");
		engine.tokenizer.advance();
		String newName = engine.tokenizer.current();
		engine.interactor.editActivityName(proj, oldActivity, newName);
	}

	//Written by Andreas
	private static void parseNextAsAssignDevToActivity(CommandEngine engine) throws ParsingException, DuplicateException, TooManyActivitiesException, NoElementException {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A developer's initials need to be given. Write '" + getSignature(ASSIGN_DEV_TO_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		String initials = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(ASSIGN_DEV_TO_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		String proj = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("An activity needs to be specified. Write '" + getSignature(ASSIGN_DEV_TO_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		String activity = engine.tokenizer.current();
		engine.interactor.assignDevToActivity(initials, proj, activity);
	}


	//Developer Niko
	private static void parseNextAsRegisterDeveloperHours(CommandEngine engine) throws ParsingException, NumberFormatException, InvalidTimeException, NoElementException {
		//Dev initials
		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("A developer's initials need to be given. Write '" + getSignature(REGISTER_DEVELOPER_HOURS_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String initials = engine.tokenizer.current();

		//Project name
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(REGISTER_DEVELOPER_HOURS_COMMAND) + "'.");
		engine.tokenizer.advance();
		String proj = engine.tokenizer.current();

		//Activity name
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("An activity needs to be specified. Write '" + getSignature(REGISTER_DEVELOPER_HOURS_COMMAND) + "'.");
		engine.tokenizer.advance();
		String activity = engine.tokenizer.current();

		//Hours
		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("An amount of hours needs to be specified. Write '" + getSignature(REGISTER_DEVELOPER_HOURS_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String hours = engine.tokenizer.current();

		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("An amount of minutes needs to be specified. Write '" + getSignature(REGISTER_DEVELOPER_HOURS_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String minutes = engine.tokenizer.current();

		engine.interactor.registerDeveloperHoursToActivity(proj, activity, initials, Integer.parseInt(hours), Integer.parseInt(minutes));
	}


	//Author Niko
	//<project id> <activity name> <start week> <start year> <end week> <end year>"
	private static void parseNextAsSetActivityTimeFrame(CommandEngine engine) throws ParsingException, NoElementException, InvalidTimeException {
		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("A project needs to be specified. Write '" + getSignature(SET_ACTIVITY_TIMEFRAME_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String projectId = engine.tokenizer.current();

		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("An activity needs to be given. Write '" + getSignature(SET_ACTIVITY_TIMEFRAME_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String activityName = engine.tokenizer.current();

		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("A start week needs to be given. Write '" + getSignature(SET_ACTIVITY_TIMEFRAME_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String startWeek = engine.tokenizer.current();

		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("A start year needs to be given. Write '" + getSignature(SET_ACTIVITY_TIMEFRAME_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String startYear = engine.tokenizer.current();


		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("An end week needs to be given. Write '" + getSignature(SET_ACTIVITY_TIMEFRAME_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String endWeek = engine.tokenizer.current();

		if(!engine.tokenizer.hasNext()) {
			throw new ParsingException("An end year needs to be given. Write '" + getSignature(SET_ACTIVITY_TIMEFRAME_COMMAND) + "'.");
		}
		engine.tokenizer.advance();
		String endYear = engine.tokenizer.current();

		int startWeekInt =-1;
		int endWeekInt =-1;
		int startYearInt =-1;
		int endYearInt =-1;


		try {
			startWeekInt = Integer.parseInt(startWeek);
		}
		catch(NumberFormatException e) {
			throw new ParsingException("Start week must be an integer");
		}

		try {
			endWeekInt = Integer.parseInt(endWeek);
		}
		catch(NumberFormatException e) {
			throw new ParsingException("End week must be an integer");
		}

		try {
			startYearInt = Integer.parseInt(startYear);
		}
		catch(NumberFormatException e) {
			throw new ParsingException("Start year must be an integer");
		}
		try {
			endYearInt = Integer.parseInt(endYear);
		}
		catch(NumberFormatException e) {
			throw new ParsingException("EndYear must be an integer");
		}

		engine.interactor.setActivityTime(projectId, activityName, startWeekInt, startYearInt, endWeekInt, endYearInt);


	}

	//Written by Andreas
	private static void parseNextAsAssignFixedActivity(CommandEngine engine) throws ParsingException, DuplicateException, NoElementException, InvalidTimeException, IllegalNameException {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A developer needs to be specified. Write '" + getSignature(ASSIGN_FIXED_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		String dev = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Activity needs a name. Write '" + getSignature(ASSIGN_FIXED_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		engine.interactor.addFixedActivity(dev, engine.tokenizer.current());
	}

	//Written by Andreas
	private static void parseNextAsRemoveFromFixedActivity(CommandEngine engine) throws ParsingException, DuplicateException, NoElementException {
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("A developer needs to be specified. Write '" + getSignature(REMOVE_FIXED_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		String dev = engine.tokenizer.current();
		if (!engine.tokenizer.hasNext())
			throw new ParsingException("Activity needs to be specified. Write '" + getSignature(REMOVE_FIXED_ACTIVITY_COMMAND) + "'.");
		engine.tokenizer.advance();
		engine.interactor.removeFixedActivity(dev, engine.tokenizer.current());
	}

	//Written by Andreas
	private static void parseNextAsHelp(CommandEngine engine) {
		System.out.println("Command list:");
		commandFunctionMap.entrySet().stream()
			.filter(e -> !e.getKey().equals(EXIT_COMMAND) && !e.getKey().equals(HELP_COMMAND))
			.forEach(e -> System.out.println(e.getValue().getSignature()));
		System.out.println("Write '" + EXIT_COMMAND + "' to terminate the program.");
	}
}
