package presentation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import business.Activity;
import business.Developer;
import business.DuplicateException;
import business.IllegalNameException;
import business.NoElementException;
import business.InvalidTimeException;
import business.Project;
import business.ProjectLeaderException;
import business.ProjectManager;
import business.Time;
import business.TooManyActivitiesException;

public class Interactor {
	
	private ProjectManager projManager;
	
	//Written by Andreas
	public Interactor(ProjectManager manager) {
		projManager = manager;
	}
	
	//Written by Andreas
	private Project getProjectByNameOrId(String str) throws ParsingException, NoElementException {
		if (str.length() < 1)
			throw new ParsingException("Project was empty.");
		if (Character.isLetter(str.charAt(0))) {
			return projManager.getProjectFromName(str);
		}
		return projManager.getProjectFromId(str);
	}
	
	//Written by Andreas
	public void addProject(String name) throws Exception {
		System.out.println("Generated project ID: " + projManager.addProject(new Project(name)));
	}
	
	//Author: Mia
	public void removeProject(String name) throws NoElementException, ParsingException {
		projManager.removeProject(getProjectByNameOrId(name));
	}
	
	//Author: Mia
	public void editProjectName(String oldName, String newName) throws NoElementException, IllegalNameException, DuplicateException {
		Project project = projManager.getProjectFromName(oldName);
		if(!projManager.containsProjectWithName(newName)){
			project.setName(newName);
		}
	
	}

	//Written by Andreas
	public void addDeveloper(String name, String initials) throws DuplicateException, IllegalNameException {
		projManager.addDeveloper(new Developer(initials, name));
	}

	//Written by Andreas
	public void addActivity(String projName, String actName) throws DuplicateException, NoElementException, InvalidTimeException, ParsingException, IllegalNameException {
		Project project = getProjectByNameOrId(projName);
		project.addActivity(new Activity(actName));
	}
	
	//Author: Mia
	public void removeActivity(String projName, String actName) throws DuplicateException, NoElementException, ParsingException {
		Project project = getProjectByNameOrId(projName);
		project.removeActivity(project.getActivityFromName(actName));
	}
	
	//Author: Mia
	public void editActivityName(String proj, String oldName, String newName) throws NoElementException, ParsingException, IllegalNameException, DuplicateException {
		Project project = getProjectByNameOrId(proj);
		Activity activity = project.getActivityFromName(oldName);
		if(!project.containsActivityWithName(newName)){
			activity.setName(newName);
		}
		else {
			throw new DuplicateException("Activity with same name already in project");

		}
		
	}

	//Written by Andreas
	public void assignDevToActivity(String initials, String projName, String activityName) throws DuplicateException, TooManyActivitiesException, NoElementException, ParsingException {
		Developer dev = projManager.getDeveloperFromId(initials);
		Project project = getProjectByNameOrId(projName);
		Activity activity = project.getActivityFromName(activityName);
		activity.addDeveloper(dev, projManager);
	}
	
	//Author: Helena
	public void addProjectLeader(String projName, String projLeader) throws ProjectLeaderException, NoElementException, ParsingException {
		Project project = getProjectByNameOrId(projName);
		Developer leader = projManager.getDeveloperFromId(projLeader);
		project.addProjectLeader(leader);
	}
	
	//Author Nikolaj
	public void resetRegisteredDevHours(String projectId, String activityName, String devId) throws NoElementException, ParsingException {
		getProjectByNameOrId(projectId).getActivityFromName(activityName).resetTime(projManager.getDeveloperFromId(devId));
	}

	//Author Nikolaj
	public void setActivityTime(String projectID, String ActivityName, int startWeek, int startYear, int endWeek, int endYear) throws NoElementException, ParsingException, InvalidTimeException {
		getProjectByNameOrId(projectID).getActivityFromName(ActivityName).setTimeFrame(startWeek,startYear,endWeek,endYear);
	}
	
	//Author Nikolaj
	public void registerDeveloperHoursToActivity(String projectId, String ActivityName, String devId, int hours, int minutes) throws ParsingException, InvalidTimeException, NoElementException {
		getProjectByNameOrId(projectId).getActivityFromName(ActivityName)
			.registerDeveloperHours(projManager.getDeveloperFromId(devId), new Time(hours, minutes));
	}
	
	//Author Nikolaj
	public void printProjects() throws InvalidTimeException {
		System.out.println("The system contains "+projManager.getProjects().size()+" projects:");
		Map<String, Project> projs = projManager.getProjects();
		for(var p : projs.keySet()) {
			Project proj = projs.get(p);
			Developer leader = proj.getProjectLeader();
			String leaderStr = leader == null ? "No leader" : leader.getInitials();
			System.out.printf("Proj Name: %-17s Id: %-10s Leader: %-11s \n", proj.getName(), p, leaderStr);
			for(Activity a : proj.getActivities()) {
				try {
					System.out.printf("\tActivity Name: %-10s No. of Devs: %-10s Expected Time: %-5s Registered Hours: %-5s Expected Hours Left: %-5s Start Week: %-5s  Start year: %-5s End week: %-5s End Year %-5s\n",
						a.getName(),a.getDevCount(),String.valueOf(a.getExpectedTime().getHours()),a.getTotalRegisteredHours(),a.getLeftoverExpectedHours(),a.getStartDate().getWeek(),a.getStartDate().getYear(),a.getEndDate().getWeek(),a.getEndDate().getYear());
				}
				catch(Exception e) {
					System.out.printf("\tActivity Name: %-10s No. of Devs: %-10s Expected Time: %-10s Registered Hours: %-5s Expected Hours Left: %-5s \n",a.getName(),a.getDevCount(),String.valueOf(a.getExpectedTime().getHours()),a.getTotalRegisteredHours(),a.getLeftoverExpectedHours());
				}
				for(Developer d : a.getAllDevelopers()) {
					Time t = a.getRegisteredDeveloperHours(d);
					System.out.printf("\t\t Developer: %-5s %-20s Hours: %-10s Minutes: %-10s\n",d.getInitials(),d.getName(),t.getHours(),t.getResidueMinutes());					
				}
				System.out.println("");		
				
			}
			System.out.println("");
		}
		
	}
	
	//Author: Helena
	public void printProject(String projectName) throws InvalidTimeException, NoElementException, ParsingException {
		try {
			Project p = getProjectByNameOrId(projectName);
			Collection<Activity> activities = p.getActivities();
			Map<String, Project> projs = projManager.getProjects();
			String projID = null;
			
			for(var p1 : projs.keySet()) {
				if(projs.get(p1).getName() == p.getName()) {
					projID = p1.toString();
				}
			}
			Developer leader = p.getProjectLeader();
			String leaderStr = leader == null ? "No leader" : leader.getInitials();
			System.out.printf("Proj Name: %-17s Id: %-10s Leader: %-11s \n", p.getName(), projID, leaderStr);
			for(Activity a : activities) {
				try {
					System.out.printf("\tActivity Name: %-10s No. of Devs: %-10s Expected Time: %-5s Registered Hours: %-5s Expected Hours Left: %-5s Start Week: %-5s  Start year: %-5s End week: %-5s End Year %-5s\n",
						a.getName(),a.getDevCount(),String.valueOf(a.getExpectedTime().getHours()),a.getTotalRegisteredHours(),a.getLeftoverExpectedHours(),a.getStartDate().getWeek(),a.getStartDate().getYear(),a.getEndDate().getWeek(),a.getEndDate().getYear());
				}
				catch(Exception e) {
					System.out.printf("\tActivity Name: %-10s No. of Devs: %-10s Expected Time: %-10s Registered Hours: %-5s Expected Hours Left: %-5s \n",a.getName(),a.getDevCount(),String.valueOf(a.getExpectedTime().getHours()),a.getTotalRegisteredHours(),a.getLeftoverExpectedHours());
				}
				for(Developer d : a.getAllDevelopers()) {
					Time t = a.getRegisteredDeveloperHours(d);
					System.out.printf("\t\t Developer: %-5s %-20s Hours: %-10s Minutes: %-10s\n",d.getInitials(),d.getName(),t.getHours(),t.getResidueMinutes());					
				}
				System.out.println("");			
			}
			System.out.println("");
		
		} catch (NoElementException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//Author Nikolaj
	public void printProjectsFromDev(String devId) throws InvalidTimeException{
		Developer d;
		try {
			d = projManager.getDeveloperFromId(devId);
			ArrayList<String> devsProjects = projManager.getDevelopersProjects(d);
			Map<String, Project> projs = projManager.getProjects();
			
			for(String p: devsProjects) {
				
				Project proj = projs.get(p);
				Developer leader = proj.getProjectLeader();
				String leaderStr = leader == null ? "No leader" : leader.getInitials();
				System.out.printf("Proj Name: %-17s Id: %-10s Leader: %-11s \n", proj.getName(), p, leaderStr);
				for(Activity a: proj.getDevelopersActivities(d)) {
					try {
						System.out.printf("\tActivity Name: %-10s No. of Devs: %-10s Expected Time: %-5s Start Week: %-5s  Start year: %-5s End week: %-5s End Year %-5s\n",
								a.getName(),a.getDevCount(),String.valueOf(a.getExpectedTime().getHours()),a.getStartDate().getWeek(),a.getStartDate().getYear(),a.getEndDate().getWeek(),a.getEndDate().getYear());
					}
					catch(Exception e) {
						System.out.printf("\tActivity Name: %-10s No. of Devs: %-10s Expected Time: %-10s\n",a.getName(),a.getDevCount(),String.valueOf(a.getExpectedTime().getHours()));
					}
					Time t = a.getRegisteredDeveloperHours(d);
					System.out.printf("\t\t Developer: %-5s %-20s Hours: %-10s Minutes: %-10s\n",d.getInitials(),d.getName(),t.getHours(),t.getResidueMinutes());
				}
			}
			System.out.println("Fixed Activities:");
			for(Activity a:d.getFixedActivities()) {
				System.out.printf("\tActivity Name: %-10s \n",
						a.getName());
			}	
		} catch (NoElementException e) {
			System.out.println(e.getMessage());
		}
	}
	
	// Author: Helena
	public void printDevelopers() {
		System.out.println("The system contains "+projManager.getDevelopers().size()+" developers:");
		List<Developer> developers = projManager.getDevelopers();
		
			for (var d : developers) {
				System.out.printf("\t\t Developer: %-5s %-20s\n",d.getInitials(),d.getName());
			}
	}

	//Author: Andreas
	public void addFixedActivity(String dev, String actName) throws DuplicateException, NoElementException, InvalidTimeException, IllegalNameException {
		projManager.getDeveloperFromId(dev).addFixedActivity(new Activity(actName));
	}
	
	//Author: Andreas
	public void removeFixedActivity(String devInit, String actName) throws DuplicateException, NoElementException {
		Developer dev = projManager.getDeveloperFromId(devInit);
		Activity act = dev.getFixedActivities().stream().filter(a -> a.getName().equals(actName)).findAny().orElse(null);
		if (act == null)
			throw new NoElementException("Activty doesn't exist.");
		dev.removeFixedActivity(act);
	}

	//Written by Andreas
	public void setExpectedTime(String project, String activityName, long hours, long minutes) throws NoElementException, InvalidTimeException, ParsingException {
		Project p = getProjectByNameOrId(project);
		Activity act = p.getActivityFromName(activityName);
		act.setExpectedTime(new Time(hours, minutes));
	}

	//Written by Andreas
	public void setProjectLeader(String project, String dev) throws NoElementException, ProjectLeaderException, ParsingException {
		Project p = getProjectByNameOrId(project);
		p.addProjectLeader(projManager.getDeveloperFromId(dev));
	}

	//Written by Andreas
	public void removeProjectLeader(String project) throws NoElementException, ProjectLeaderException, ParsingException {
		Project p = getProjectByNameOrId(project);
		p.removeProjectLeader();	
	}
	
}
