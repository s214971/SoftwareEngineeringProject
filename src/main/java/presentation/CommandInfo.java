package presentation;

/**
 * Written by Andreas.
 * 
 * Immutable.
 */
public class CommandInfo {
	private CommandExecutable command;
	private String signature;
	
	public CommandInfo(CommandExecutable command, String signature) {
		this.command = command;
		this.signature = signature;
	}

	public String getSignature() {
		return signature;
	}
	
	public void run(CommandEngine engine) throws Exception {
		command.run(engine);
	}
}
