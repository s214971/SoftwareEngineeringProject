package business;

public class TooManyActivitiesException extends PlanningAppException {

	public TooManyActivitiesException(String errorMessage) {
		super(errorMessage);
	}

}
