package business;

/**
 * Immutable.
 */
public class Time {
	private static final int MIN_IN_HR = 60;
	private static final int SEC_IN_MIN = 60;
	private static final int MILLIS_IN_SEC = 1000;
	private static final String INVALID_TIME_MESSAGE = "Invalid time.";

	private long millis = 0;

	//Written by Andreas
	public Time(long hours) throws InvalidTimeException {
		millis += hours * MIN_IN_HR * SEC_IN_MIN * MILLIS_IN_SEC;
		checkTimeValidity();
	}

	//Written by Andreas
	public Time(long hours, long minutes) throws InvalidTimeException {
		this(hours);
		millis += minutes * SEC_IN_MIN * MILLIS_IN_SEC;
		checkTimeValidity();
	}

	//Written by Andreas
	public Time(long hours, long minutes, long seconds) throws InvalidTimeException {
		this(hours, minutes);
		millis += seconds * MILLIS_IN_SEC;
		checkTimeValidity();
	}

	//Written by Andreas
	private void checkTimeValidity() throws InvalidTimeException {
		if (millis < 0)
			throw new InvalidTimeException(INVALID_TIME_MESSAGE);
	}

	//Written by Andreas
	public long getHours() {
		return millis / (MILLIS_IN_SEC * SEC_IN_MIN * MIN_IN_HR);
	}


	//Written by Andreas
	public long getMillis() {
		return millis;
	}

	//Written by Nikolaj
	public void addTimes(Time other) throws InvalidTimeException {
		millis += other.getMillis();
	}

	//Written by Nikolaj
	public int getResidueMinutes() {
		return (int) ((millis % (MILLIS_IN_SEC * SEC_IN_MIN * MIN_IN_HR))/(MILLIS_IN_SEC * SEC_IN_MIN));
	}
}
