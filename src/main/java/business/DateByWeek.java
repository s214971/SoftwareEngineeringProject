package business;

/**
 * Written by Andreas.
 * 
 * Immutable.
 */
public class DateByWeek {
	
	private static final int WEEKS_IN_YEAR = 52;
	
	int year;
	int week;
	
	public DateByWeek(int year, int week) {
		assert week > 0 && week <= WEEKS_IN_YEAR;
		this.year = year;
		this.week = week;
	}
	
	public int getYear() { return year; }
	public int getWeek() { return week; }
}