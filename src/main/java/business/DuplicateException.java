package business;

//Author: Mia
public class DuplicateException extends PlanningAppException {
	
	public DuplicateException(String errorMessage) {
		super(errorMessage);
		
	}
}
