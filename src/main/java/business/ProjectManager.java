package business;

import java.util.*;

public class ProjectManager {
	private ArrayList<Developer> developers;
	/**
	 * Duplicate projects may not exist.
	 */
	private Map<String, Project> projects;
	private Map<String, Project> archive;
	private IDGenerator idGenerator;
	
	//Written by Andreas
	public ProjectManager() {
		this.developers = new ArrayList<Developer>();
		projects = new HashMap<>();
		archive = new HashMap<>();
		idGenerator = new IDGenerator(new TimeServer());
	}
	
	//Written by Andreas
	public void setTimeServer(TimeServer server) {
		assert server != null;
		idGenerator.setTimeServer(server);
	}
	
	public void addDeveloper(Developer dev) throws DuplicateException {
		if(containsDeveloperWithId(dev.getInitials())) {
			throw new DuplicateException("There is already a developer with ID: "+dev.getInitials()+" in system");
		}
		developers.add(dev);
	}
	
	public boolean containsDeveloper(Developer dev) {
		return developers.contains(dev);
	}
	
	//Written by Andreas
	public boolean containsDeveloperWithId(String id) {
		for (Developer developer : developers) {
			if(developer.getInitials().equals(id)) {
				return true;
			}
		}		
		return false;
	}
	
	//Author Nikolaj
	//Pre-conditions:
	//The list of developers cannot be null
	//The passed id, cannot be null
	
	//Post-conditions:
	//Returns not-null-developer with id = passed id
	//OR returns error, if no such developer exists
	public Developer getDeveloperFromId(String id) throws NoElementException {
		assert developers != null;
		assert id != null;
		
		for (Developer developer : developers) {
			if(developer.getInitials().equals(id)) {
				assert developer != null && developer.getInitials() == id;
				return developer;
			}
		}
		assert developers.stream().anyMatch(d->d.getInitials().equals(id)) == false;
		throw new NoElementException("No developer with initials: "+id);
	}
	
	//Written by Andreas
	public boolean containsProject(Project proj) {
		return projects.containsValue(proj);
	}
	
	/** Written by Andreas
	 * Preconditions:
	 * - proj != null && proj.getName() != null && projects != null && idGenerator != null
	 * 		&& all projects in projects are non-null and have non-null names
	 * Postconditions:
	 * - if a project in projects has the same name as proj, then throw DuplicateException.
	 * - else, create a unique String ID for proj, add proj to projects and return its ID.
	 */
	public String addProject(Project proj) throws DuplicateException {
		assert proj != null && proj.getName() != null && projects != null && idGenerator != null 
				&& !projects.values().stream().anyMatch(p -> p == null || p.getName().equals(null));
		if (projects.containsValue(proj)) {
			assert projects.values().stream().anyMatch(p -> p.getName().equals(proj.name));
			throw new DuplicateException("Project object already in project manager");
		}
		if (projects.values().stream().anyMatch(p -> p.getName().equals(proj.name))) {
			assert projects.values().stream().anyMatch(p -> p.getName().equals(proj.name));
			throw new DuplicateException("Project with same name already in project manager");
		}
		String id = idGenerator.getNextID();
		assert !projects.containsKey(id) 
			&& !projects.values().stream().anyMatch(p -> p.getName().equals(proj.name));
		projects.put(id, proj);
		assert projects.get(id) == proj;
		return id;
	}
	
	//Written by Andreas
	public Project getProjectFromId(String id) throws NoElementException{
		if(projects.containsKey(id)) 
			return projects.get(id);
		throw new NoElementException("No project with Id: "+id);
	}
	
	//Written by Andreas
	public Project getProjectFromName(String name) throws NoElementException{
		for (Project project : projects.values()) 
			if(project.getName().equals(name)) 
				return project;
		throw new NoElementException("No project with name: "+name);
	}
	
	/* Written by Andreas
	 * This relies on the fact that duplicate objects may not exist in projects.
	 */
	public void removeProject(Project project) throws NoElementException {
		assert project != null && projects != null && archive != null;
		String keyToRemove = null;
		boolean projectRemoved = false;	//Used to check if project doesn't exist 
										//and to check the no-duplicate invariant.
		for (String key : projects.keySet()) {
			Project proj = projects.get(key);
			if (proj == project) {
				assert !projectRemoved : "Project manager contains duplicate projects";
				keyToRemove = key;
				archive.put(key, proj);
				projectRemoved = true;
				//The loop is intentionally continued in order to check if the
				//duplicate invariant is broken.
			}
		}
		if (!projectRemoved) {
			assert !containsProject(project);
			throw new NoElementException("Project doesn't exist in the system");
		}
		else
			projects.remove(keyToRemove);
		assert !containsProject(project);
	}
	
	//Written by Andreas
	public int getDeveloperActivityCount(Developer dev) {
		return projects.values().stream()
				.mapToInt(p -> (int)p.activities.stream()
						.filter(a -> a.containsDeveloper(dev))
						.count())
				.sum();
	}
	
	
	//Author: Nikolaj
	public ArrayList<String> getDevelopersProjects(Developer d){
		ArrayList<String> projectsWithDev = new ArrayList<>();
		for(String p: projects.keySet()) {
			if(projects.get(p).containsDeveloper(d)){
				projectsWithDev.add(p);
			}
		}
		return projectsWithDev;
	}
	
	//Written by Andreas
	public Map<String, Project> getProjects() {
		return Collections.unmodifiableMap(projects);
	}
	
	// Author: Helena
	public List<Developer> getDevelopers() {
		return Collections.unmodifiableList(developers);
	}

	//Written by Andreas
	public boolean containsProjectWithId(String id) {
		return projects.containsKey(id);
	}

	//Author: Mia
	public void archiveContainsProject(Project project) {
		archive.containsValue(project);
	}

	public boolean containsProjectWithName(String proj) throws DuplicateException {
		if (projects.values().stream().anyMatch(p -> p.getName().equals(proj))) {
			throw new DuplicateException("Project with same name already in project manager");
		}
		return false;
	}
}
