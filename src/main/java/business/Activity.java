package business;

import java.util.*;

public class Activity {

	private static final int MAX_ACTIVITIES = 20;

	// Author: Mia
	private String name;
	private ArrayList<Developer> assignedDevelopers = new ArrayList<Developer>();
	private HashMap<Developer,Time> developerHours = new HashMap<Developer,Time>();
	private Time expectedTime;

	private DateByWeek startDate, endDate;

	//Written by Helena
	public Activity(String name) throws InvalidTimeException, IllegalNameException {
		setName(name);
		expectedTime = new Time(0, 0);
	}

	//Written by Helena
	public String getName() {
		return name;
	}

	//Author: Mia
	public boolean containsDeveloper(Developer dev) {
		return (assignedDevelopers.contains(dev) || developerHours.keySet().contains(dev));
	}

	//Author: Mia
	public boolean containsDeveloperWithId(String id) {
		for (Developer developer : assignedDevelopers) {
			if(developer.getInitials().equals(id)) {
				return true;
			}
		}
		return false;
	}

	//Author: Mia
	public void addDeveloper(Developer dev, ProjectManager environment) throws DuplicateException, TooManyActivitiesException {
		if (environment.getDeveloperActivityCount(dev) >= MAX_ACTIVITIES)
			throw new TooManyActivitiesException("Developer has reached the maximum amount of activities");
		if(!containsDeveloperWithId(dev.getInitials())) {
			assignedDevelopers.add(dev);
		} else {
			throw new DuplicateException("Developer is already added to the activity");
		}
	}

	//Author: Mia
	public void setName(String activityName) throws IllegalNameException {
		if (activityName == null || activityName.equals(""))
			throw new IllegalNameException("Activity name null or empty.");
		this.name = activityName;
	}

	//Author Nikolaj
	public Time getExpectedTime() {
		return expectedTime;
	}

	//Author Nikolaj
	public void setExpectedTime(Time time) {
		expectedTime = time;
	}

	//Author Nikolaj
	public int getDevCount() {
		return assignedDevelopers.size();
	}

	//Author Nikolaj
	public void setTimeFrame(int startWeek, int startYear, int endWeek, int endYear) throws InvalidTimeException {
		if(startWeek<=0 ||endWeek<=0) {
			throw new InvalidTimeException("The week number must be a positive integer");
		}
		if(startWeek>53 || endWeek>53) {
			throw new InvalidTimeException("Week numbers may be no larger than 53");
		}
		
		if(startYear<0 ||endYear<0) {
			throw new InvalidTimeException("The year must be a positive integer");
		}

		if(startYear>endYear || ((startYear==endYear) && (startWeek>endWeek))) {
			throw new InvalidTimeException("The end time cannot be before the start time");
		}

		startDate = new DateByWeek(startYear, startWeek);
		endDate = new DateByWeek(endYear, endWeek);
	}

	//Author Nikolaj
	public void registerDeveloperHours(Developer dev, Time t) throws InvalidTimeException {
		if(developerHours.containsKey(dev)) {
			developerHours.get(dev).addTimes(t);
		}
		else {
			developerHours.put(dev, t);
		}
	}

	//Author Nikolaj
	public Set<Developer> getRegisteredDevelopers() {
		return developerHours.keySet();
	}

	//Author Andreas
	public List<Developer> getAssignedDevelopers(){
		return Collections.unmodifiableList(assignedDevelopers);
	}

	//Author Andreas
	public List<Developer> getAllDevelopers(){
		ArrayList<Developer> allDevs = new ArrayList<Developer>(assignedDevelopers);
		developerHours.keySet().stream().forEach(d -> { if (!allDevs.contains(d)) allDevs.add(d); });
		return allDevs;
	}

	//Author Nikolaj
	public Time getRegisteredDeveloperHours(Developer dev) throws InvalidTimeException {
		if(developerHours.containsKey(dev)) {
			return developerHours.get(dev);
		}
		return new Time(0);
	}

	//Author Andreas
	public DateByWeek getStartDate() {
		return startDate;
	}

	//Author Andreas
	public DateByWeek getEndDate() {
		return endDate;
	}
	
	//Author Nikolaj
	public int getTotalRegisteredHours() throws InvalidTimeException {
		Time t;
		t = new Time(0,0);
		for(Developer d: developerHours.keySet()) {
			t.addTimes(developerHours.get(d));
		}
		return (int) t.getHours();
	}
	
	//Author Nikolaj
	public String getLeftoverExpectedHours() throws InvalidTimeException {
		assert expectedTime != null;
		return Long.toString(getExpectedTime().getHours() - getTotalRegisteredHours());
	}

	//Author Nikolaj
	public void resetTime(Developer developer) {
		if(developerHours.containsKey(developer)) {
			developerHours.remove(developer);
		}
	}

	
}
