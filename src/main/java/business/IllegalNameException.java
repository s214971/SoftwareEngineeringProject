package business;

//Author: Helena

public class IllegalNameException extends PlanningAppException {

	public IllegalNameException(String errorMessage) {
		super(errorMessage);
	}
}
