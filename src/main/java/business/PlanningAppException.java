package business;

//Author: Helena

public class PlanningAppException extends Exception {

	public PlanningAppException(String errorMessage) {
		super(errorMessage);
		
	}
}
