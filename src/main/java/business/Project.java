package business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.NoSuchElementException;

public class Project {

	ArrayList<Activity> activities;
	ArrayList<Activity> deletedActivities;
	String name;
	Developer projectLeader = null;

	// Author: Helena
	public Project(String name) throws IllegalNameException {
		setName(name);
		this.activities = new ArrayList<Activity>();
		this.deletedActivities = new ArrayList<Activity>();
	}

	//Author: Mia
	public void addActivity(Activity act) throws DuplicateException {
		assert activities != null;
		assert act != null;

		if(containsActivityWithName(act.getName())) {
			assert containsActivityWithName(act.getName());
			throw new DuplicateException("Duplicate activity in project " + this.getName());
		}
		this.activities.add(act);
		assert containsActivityWithName(act.getName());
	}

	//Author: Helena
	public void removeActivity(Activity act) throws NoElementException {
		if(activities.contains(act)) {
			this.deletedActivities.add(act);
			this.activities.remove(act);
		} else {
			throw new NoElementException("Activity does not exist");
		}
	}

	/*Author: Helena

	  Pre: name != null && activities != null

	  Post: activity != null && activity.getName() == name
	  		OR
	  		activities.stream().anyMatch(a->a.getName().equals(name)) == false
	 */
	public Activity getActivityFromName(String name) throws NoElementException {
	    assert name != null && activities != null : "Precondition";
		for (Activity activity : activities) {
			if(activity.getName().equals(name)){
				assert activity != null && activity.getName() == name : "Postcondition";
				return activity;
			}
		}
		assert activities.stream().anyMatch(a->a.getName().equals(name)) == false : "Postcondition";
		throw new NoElementException("No Activity with ID: "+name+" in Project: "+this.name);
	}

	//Written by Nikolaj
	public void registerDevHoursToActivity(Developer dev, Time t, String activityId) throws NoElementException, InvalidTimeException {
		getActivityFromName(activityId).registerDeveloperHours(dev, t);
	}

	//Author: Helena
	public void addProjectLeader(Developer leader) throws ProjectLeaderException {
		if (projectLeader == null) {
			this.projectLeader = leader;
		} else {
			throw new ProjectLeaderException("Project has already been assigned a leader");
		}
	}

	//Author: Helena
	public void removeProjectLeader() throws ProjectLeaderException {
		if (!(projectLeader == null)) {
			this.projectLeader = null;
		} else {
			throw new ProjectLeaderException("Project does not have a leader");
		}
	}

	public String getName() {
		return this.name;
	}

	public boolean containsActivity(Activity a) {
		return activities.contains(a);
	}

	//Written by Andreas
	public boolean containsActivityWithName(String name) {
		return activities.stream().anyMatch(a -> a.getName().equals(name));
	}

	//Author: Helena
	public boolean containsDeletedActivityWithName(String name) {
		for (Activity activity : deletedActivities) {
			if(activity.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public Developer getProjectLeader() {
		return projectLeader;
	}
	
	// Author: Helena
	public void setName(String projectName) throws IllegalNameException {
		if (projectName == null || projectName.equals("")) {
			throw new IllegalNameException("Project needs a name");
		}
		if (Character.isLetter(projectName.charAt(0))){
			this.name = projectName;
		} else {
			throw new IllegalNameException("The project name must begin with a letter.");
		}
	}

	//Written by Andreas
	public Collection<Activity> getActivities() {
		return Collections.unmodifiableCollection(activities);
	}

	//Written by Andreas
	public Collection<Activity> getRemovedActivities() {
		return Collections.unmodifiableCollection(deletedActivities);
	}

	//Written by Nikolaj
	public boolean containsDeveloper(Developer d) {
		for(Activity a: activities) {
			if(a.containsDeveloper(d)) {
				return true;
			}
		}
		return false;
	}

	//Written by Andreas
	public ArrayList<Activity> getDevelopersActivities(Developer d){
		ArrayList<Activity> devActivities = new ArrayList<>();
		for(Activity a : activities)
			if (a.containsDeveloper(d))
				devActivities.add(a);
		return devActivities;
	}
}
