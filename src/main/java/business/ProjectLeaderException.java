package business;

//Author: Helena

public class ProjectLeaderException extends PlanningAppException {
	
	public ProjectLeaderException(String errorMessage) {
		super(errorMessage);
		
	}

}
