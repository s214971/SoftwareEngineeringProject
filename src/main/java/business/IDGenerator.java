package business;

import java.util.*;

/**
 * 
 * Written by Andreas
 *
 */
public class IDGenerator {
	
	private Map<Integer, Integer> yearIDCounterMap;
	private TimeServer timeServer;
	
	public IDGenerator(TimeServer timeServer) {
		setTimeServer(timeServer);
		yearIDCounterMap = new HashMap<>();
	}
	
	public String getNextID() {
		int yearValue = timeServer.getYear();
		if (!yearIDCounterMap.containsKey(yearValue))
			yearIDCounterMap.put(yearValue, 1);
		int counter = yearIDCounterMap.get(yearValue);
		yearIDCounterMap.put(yearValue, counter + 1);
		if (yearValue < 10)
			yearValue = 100;
		String yearStr = Integer.toString(yearValue);
		String latterYear = "" + yearStr.charAt(yearStr.length() - 2) + yearStr.charAt(yearStr.length() - 1);
		return latterYear + String.format("%04d", counter);
	}

	public void setTimeServer(TimeServer server) {
		assert server != null;
		timeServer = server;
	}
}