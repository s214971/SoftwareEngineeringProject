package business;

import java.util.*;

public class Developer {
	
	private String initials;
	private String name;
	private List<Activity> fixedActivities;

	//Author: Helena
	public Developer(String initials, String name) throws IllegalNameException {
		if (initials.length() <= 4 && initials.matches("[a-zA-Z]+")) {
			this.initials = initials;
			this.setName(name);
		} else {
			throw new IllegalNameException("Developer ID has to be letters and no more than 4 characters");
		}
		fixedActivities = new ArrayList<>();
	}
	
	//Author: Mia
	public String getInitials() {
		return initials;
	}

	//Written by Nikolaj
	public String getName() {
		return name;
	}

	//Written by Nikolaj
	public void setName(String name) {
		this.name = name;
	}
	
	//Written by Andreas
	public void addFixedActivity(Activity act) throws DuplicateException {
		if (fixedActivities.contains(act) || fixedActivities.stream().anyMatch(a -> a.getName().equals(act.getName())))
			throw new DuplicateException("Fixed activity already exists for user");
		fixedActivities.add(act);
	}

	//Written by Andreas
	public List<Activity> getFixedActivities() {
		return Collections.unmodifiableList(fixedActivities);
	}

	//Written by Andreas
	public void removeFixedActivity(Activity activity) throws NoElementException {
		if (fixedActivities.contains(activity))
			fixedActivities.remove(activity);
		else
			throw new NoElementException("Fixed activity doesn't exist");
	}
}
