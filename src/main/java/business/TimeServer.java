package business;

/**
 * Written by Andreas.
 */
public class TimeServer {
	
	private final double MILLIS_IN_YEAR = 1000.0 * 60 * 60 * 24 * 365;
	private final int UNIX_TIME_START = 1970;
	
	public TimeServer() {
	}
	
	public int getYear() {
		return (int)((double)System.currentTimeMillis() / MILLIS_IN_YEAR) + UNIX_TIME_START;
	}
}
