package business;

//Author: Helena

public class NoElementException extends PlanningAppException {

	public NoElementException(String errorMessage) {
		super(errorMessage);
	}

}
