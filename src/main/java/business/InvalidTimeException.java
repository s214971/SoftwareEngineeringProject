package business;

//Author: Helena

public class InvalidTimeException extends PlanningAppException {

	public InvalidTimeException(String errorMessage) {
		super(errorMessage);
	}

}
