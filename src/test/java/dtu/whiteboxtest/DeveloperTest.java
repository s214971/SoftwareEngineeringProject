package dtu.whiteboxtest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import business.Activity;
import business.Developer;
import business.DuplicateException;
import business.IllegalNameException;
import business.NoElementException;
import business.Project;
import business.ProjectManager;
import business.TooManyActivitiesException;

/**
 * Written by Andreas
 */
public class DeveloperTest {
	
	@Test
	public void getDevNameTest() throws IllegalNameException {
		Developer d = new Developer("init", "name");
		assertEquals("name", d.getName());
	}	
	
	@Test
	public void getDevsTest() throws IllegalNameException, DuplicateException {
		ProjectManager pm = new ProjectManager();
		Developer d = new Developer("init", "name");
		pm.addDeveloper(d);
		assertTrue(pm.getDevelopers().contains(d));
	}	
}