package dtu.whiteboxtest;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import javax.naming.InvalidNameException;

import org.junit.Test;
import business.*;

/**
 * 
 * Written by Andreas
 *
 */
public class ProjectTest {
	
	@Test
	public void removeActivityFromProjectTest() throws IllegalNameException, DuplicateException, NoElementException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		assertFalse(p.containsDeletedActivityWithName("placeholder"));
		assertFalse(p.containsActivity(a));
		assertFalse(p.containsActivityWithName("placeholder"));
		p.addActivity(a);
		assertTrue(p.containsActivity(a));
		assertTrue(p.containsActivityWithName("placeholder"));
		assertFalse(p.containsDeletedActivityWithName("placeholder"));
		p.removeActivity(a);
		assertFalse(p.containsActivity(a));
		assertFalse(p.containsActivityWithName("placeholder"));
		assertTrue(p.containsDeletedActivityWithName("placeholder"));
	}	
	
	@Test
	public void removeNonExistentActivityFromProjectTest() throws IllegalNameException, DuplicateException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		assertFalse(p.containsDeletedActivityWithName("placeholder"));
		assertFalse(p.containsActivity(a));
		assertFalse(p.containsActivityWithName("placeholder"));
		try {
			p.removeActivity(a);
			fail();
		} catch(NoElementException ex) {
			
		}
		assertFalse(p.containsActivity(a));
		assertFalse(p.containsActivityWithName("placeholder"));
		assertFalse(p.containsDeletedActivityWithName("placeholder"));
	}	
	
	@Test
	public void accessNonExistentActivityInProjectTest() throws IllegalNameException, NoElementException, DuplicateException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		assertFalse(p.containsActivity(a));
		
		try {
			p.getActivityFromName(a.getName());
			fail();
		} catch(NoElementException ex) {
			
		}
	}	
	
	@Test
	public void accessActivityInProjectTest() throws IllegalNameException, NoElementException, DuplicateException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		
		p.addActivity(a);
		
		assertEquals(a, p.getActivityFromName(a.getName()));
	}	
	
	@Test
	public void containsDevInProjectTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		Developer dev = new Developer("init", "name");
		ProjectManager pm = new ProjectManager();
		
		p.addActivity(a);
		a.addDeveloper(dev, pm);
		
		assertTrue(p.containsDeveloper(dev));
	}	
	
	@Test
	public void doesNotContainDevInProjectTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException {
		Project p = new Project("placeholder");
		Developer dev = new Developer("init", "name");
		
		assertFalse(p.containsDeveloper(dev));
	}	
	
	@Test
	public void getDevActivitiesInProjectTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		Developer dev = new Developer("init", "name");
		ProjectManager pm = new ProjectManager();
		
		p.addActivity(a);
		a.addDeveloper(dev, pm);
		
		ArrayList<Activity> l = p.getDevelopersActivities(dev);
		assertTrue(l.contains(a));
	}	
	
	@Test
	public void getDevActivitiesThatHasNoActivitiesInProjectTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		Developer dev = new Developer("init", "name");
		
		p.addActivity(a);
		
		ArrayList<Activity> l = p.getDevelopersActivities(dev);
		assertFalse(l.contains(a));
	}	
	
	@Test
	public void invalidNameTest() {
		try {
			Project p = new Project("1project");			
		} catch (IllegalNameException ex) {
			assertEquals("The project name must begin with a letter.", ex.getMessage());
			return;
		}
		fail();
	}	
	
	//Author: Mia
	@Test
	public void registerDevHoursToActivityTest() throws IllegalNameException, InvalidTimeException, NoElementException, DuplicateException {
		Project p = new Project("project");
		Activity a = new Activity("activity");
		p.addActivity(a);
		Developer d = new Developer("init", "name");
		Time t = new Time(10,0);
		p.registerDevHoursToActivity(d,t,"activity");
		assertEquals(a.getRegisteredDeveloperHours(d).getMillis(),t.getMillis());
	}
}