package dtu.whiteboxtest;

import org.junit.Test;
import static org.junit.Assert.*;

import business.Activity;
import business.Developer;
import business.DuplicateException;
import business.IllegalNameException;
import business.InvalidTimeException;
import business.NoElementException;
import business.Project;
import business.ProjectManager;

// Author: Helena

public class WhiteBoxTestHelena {
	
	// 0 activities, no match
	@Test
	public void getActivityFromNameA() throws NoElementException, IllegalNameException, DuplicateException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("projectName");
		pm.addProject(p);
			try {
				p.getActivityFromName("importantActivity");
			} catch (Exception e) {
				assertEquals("No Activity with ID: importantActivity in Project: projectName",e.getMessage());
				return;
			}
			fail();
	}
	
	// 1 activity, no match
	@Test
	public void getActivityFromNameB() throws NoElementException, DuplicateException, IllegalNameException, InvalidTimeException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("projectName");
		pm.addProject(p);
		Activity act1 = new Activity("Activity1");
		p.addActivity(act1);
		
			try {
				p.getActivityFromName("importantActivity");
			} catch (Exception e) {
				assertEquals("No Activity with ID: importantActivity in Project: projectName",e.getMessage());
				return;
			}
			fail();
	}
	
	// 1 activity, match
	@Test
	public void getActivityFromNameC() throws DuplicateException, IllegalNameException, NoElementException, InvalidTimeException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("projectName");
		pm.addProject(p);
		Activity impAct = new Activity("importantActivity");
		p.addActivity(impAct);
		Activity impAct2 = p.getActivityFromName("importantActivity");
		assertEquals(impAct,impAct2);	
	}
	

}
