package dtu.whiteboxtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import business.Developer;
import business.DuplicateException;
import business.IllegalNameException;
import business.InvalidTimeException;
import business.NoElementException;
import business.Project;
import business.ProjectManager;
import business.Time;

public class WhiteBoxTestNikolaj {
	
	//Author Nikolaj
	
	//1 Dev Match!
	@Test
	public void testProjectFromIdA() throws NoElementException, IllegalNameException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("A");
		try {
			pm.addProject(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Project p2 = pm.getProjectFromName("A");
		assertEquals(p,p2);
	}
	
	//1 Dev no match
	@Test
	public void testProjectFromIdB() throws IllegalNameException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("A");
		try {
			pm.addProject(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			pm.getProjectFromName("B");
		}
		catch(NoElementException e) {
			assertEquals("No project with name: B",e.getMessage());
			return;
		}
		fail();
	}
	
	//0 devs, no match
	@Test
	public void testProjectFromIdC() {
		ProjectManager pm = new ProjectManager();
		try {
			pm.getProjectFromName("B");
		}
		catch(Exception e) {
			assertEquals("No project with name: B",e.getMessage());
			return;
		}
		fail();
	}
	
	
	//1 Developer, Match
	@Test
	public void testDeveloperFromIdA() throws IllegalNameException, DuplicateException, NoElementException {
		ProjectManager pm = new ProjectManager();
		Developer d = new Developer("A","AAA");
		pm.addDeveloper(d);
		Developer d2 = pm.getDeveloperFromId("A");
		assertEquals(d,d2);		
	}
	
	//1 Developer, No match
	@Test
	public void testDeveloperFromIdB() throws DuplicateException, IllegalNameException {
		ProjectManager pm = new ProjectManager();
		Developer d = new Developer("A","AAA");
		pm.addDeveloper(d);
			
		try {
			pm.getDeveloperFromId("B");		}
		catch(NoElementException e) {
			assertEquals("No developer with initials: B",e.getMessage());
			return;
		}
		fail();
	}
	
	//0 Developers, No match
	@Test
	public void testDeveloperFromIdC() {
		ProjectManager pm = new ProjectManager();
		try {
			pm.getDeveloperFromId("B");		}
		catch(Exception e) {
			assertEquals("No developer with initials: B",e.getMessage());
			return;
		}
		fail();
	}
	
	
	
	//Test of Time.getResidueMinutes
	@Test
	public void getResidueMinutesTest() {
		try {
			assert(new Time(10,0).getResidueMinutes()==0);
			assert(new Time(10,5).getResidueMinutes()==5);
			assert(new Time(10,61).getResidueMinutes()==1);
		} catch (InvalidTimeException e) {
			fail();
		}
	}
}
