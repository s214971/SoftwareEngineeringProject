package dtu.whiteboxtest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import business.Activity;
import business.Developer;
import business.DuplicateException;
import business.IllegalNameException;
import business.InvalidTimeException;
import business.NoElementException;
import business.Project;
import business.ProjectManager;
import business.Time;
import business.TooManyActivitiesException;

/**
 * 
 * Written by Andreas.
 *
 */
public class ActivityTest {
	
	@Test
	public void activityNameInvalidTest() throws InvalidTimeException {
		try {
			new Activity("");
		} catch (IllegalNameException ex) {
			assertEquals("Activity name null or empty.", ex.getMessage());
			return;
		}
		fail();
	}
	
	@Test
	public void activityNameNullTest() throws InvalidTimeException {
		try {
			new Activity(null);
		} catch (IllegalNameException ex) {
			assertEquals("Activity name null or empty.", ex.getMessage());
			return;
		}
		fail();
	}
	
	@Test
	public void getDevsEmptyTest() throws IllegalNameException, DuplicateException, NoElementException, InvalidTimeException {
		Activity a = new Activity("placeholder");
		assertEquals(0, a.getAllDevelopers().size());
		assertEquals(0, a.getAssignedDevelopers().size());
		assertEquals(0, a.getRegisteredDevelopers().size());
	}
	
	@Test
	public void getDevsTest() throws IllegalNameException, DuplicateException, NoElementException, TooManyActivitiesException, InvalidTimeException {
		Activity a = new Activity("placeholder");
		Developer d = new Developer("dev", "dev");
		a.addDeveloper(d, new ProjectManager());
		
		assertEquals(1, a.getAllDevelopers().size());
		assertEquals(1, a.getAssignedDevelopers().size());
		assertEquals(0, a.getRegisteredDevelopers().size());
		
		a.registerDeveloperHours(d, new Time(2));
		
		assertEquals(1, a.getAllDevelopers().size());
		assertEquals(1, a.getAssignedDevelopers().size());
		assertEquals(1, a.getRegisteredDevelopers().size());
		
		Developer d2 = new Developer("deve", "devtwo");
		a.registerDeveloperHours(d2, new Time(1));
		
		assertEquals(2, a.getAllDevelopers().size());
		assertEquals(1, a.getAssignedDevelopers().size());
		assertEquals(2, a.getRegisteredDevelopers().size());
	}
	
}