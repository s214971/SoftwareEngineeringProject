package dtu.whiteboxtest;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import business.*;

/**
 * 
 * Written by Andreas
 *
 */

public class ProjectManagerTest {
	
	@Test
	public void addDuplicateProjectTest() throws IllegalNameException, DuplicateException, NoElementException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("placeholder");
		pm.addProject(p);
		try {
			pm.addProject(p);
		} catch (DuplicateException e) {
			assertEquals("Project object already in project manager", e.getMessage());
			return;
		}
		fail();
	}
	
	@Test
	public void addDuplicateNameProjectTest() throws IllegalNameException, DuplicateException, NoElementException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("placeholder");
		pm.addProject(p);
		try {
			pm.addProject(new Project("placeholder"));
		} catch (DuplicateException e) {
			assertEquals("Project with same name already in project manager", e.getMessage());
			return;
		}
		fail();
	}
	
	@Test
	public void addNoNameProjectTest() throws IllegalNameException, DuplicateException, NoElementException {
		ProjectManager pm = new ProjectManager();
		try {
			Project p = new Project("");
			pm.addProject(p);
		} catch (IllegalNameException e) {
			assertEquals("Project needs a name", e.getMessage());
			return;
		}
		fail();
	}
	
	@Test
	public void addProjectSuccessfullyTest() throws IllegalNameException, DuplicateException, NoElementException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("placeholder");
		String id = pm.addProject(p);
		assertTrue(pm.containsProject(p));
		assertEquals(p, pm.getProjectFromId(id));
	}
	
	@Test
	public void removeProjectSuccessfullyTest() throws IllegalNameException, DuplicateException, NoElementException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("placeholder");
		pm.addProject(p);
		assertTrue(pm.containsProject(p));
		pm.removeProject(p);
		assertFalse(pm.containsProject(p));
	}
	
	@Test
	public void removeNonExistentProjectTest() throws IllegalNameException, DuplicateException {
		ProjectManager pm = new ProjectManager();
		Project p = new Project("placeholder");

		try {
			pm.removeProject(p);
		} catch (NoElementException e) {
			assertFalse(pm.containsProject(p));
			return;
		}
		fail();
	}

	@Test
	public void getDevProjectsTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException, NoElementException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		Developer dev = new Developer("init", "name");
		ProjectManager pm = new ProjectManager();
		
		String id = pm.addProject(p);
		p.addActivity(a);
		a.addDeveloper(dev, pm);
		
		ArrayList<String> l = pm.getDevelopersProjects(dev);
		assertTrue(l.contains(id));
		assertEquals(p, pm.getProjectFromId(l.get(0)));
	}	
	
	@Test
	public void getContainsProjectNameTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException, NoElementException, InvalidTimeException {
		Project p = new Project("placeholder");
		ProjectManager pm = new ProjectManager();
		
		assertFalse(pm.containsProjectWithName(p.getName()));
		
		pm.addProject(p);
		
		try {
			pm.containsProjectWithName(p.getName());
		} catch (DuplicateException ex) {
			
			assertEquals("Project with same name already in project manager", ex.getMessage());
			return;
		}
		fail();
	}
	
	
	@Test
	public void getDevProjectsWithProjectInCollectionTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException, NoElementException, InvalidTimeException {
		Project p = new Project("placeholder");
		Activity a = new Activity("placeholder");
		Developer dev = new Developer("init", "name");
		ProjectManager pm = new ProjectManager();
		
		a.addDeveloper(new Developer("some", "thing"), pm);
		p.addActivity(a);
		
		ArrayList<String> l = pm.getDevelopersProjects(dev);
		assertEquals(0, l.size());
	}
	
	@Test
	public void getNonExistentProjectTest() throws TooManyActivitiesException, IllegalNameException, DuplicateException, NoElementException {
		ProjectManager pm = new ProjectManager();
		assertFalse(pm.containsProjectWithId("not an id"));
		try {
			pm.getProjectFromId("not an id");
			fail();
		} catch(NoElementException ex) {
			
		}
	}	
}
