package dtu.whiteboxtest;

import static org.junit.Assert.*;

import org.junit.Test;

import business.Activity;
import business.DuplicateException;
import business.IllegalNameException;
import business.InvalidTimeException;
import business.Project;

//Author: Mia
public class WhiteBoxTestMia {
	
	@Test
	public void addDuplicateActivityTest() throws DuplicateException, InvalidTimeException, IllegalNameException {
		Project p = new Project("proj");
		Activity a = new Activity("act");
		p.addActivity(a);
		try {
			p.addActivity(a);
		} catch (DuplicateException e) {
			assertEquals("Duplicate activity in project proj", e.getMessage());
			return;
		}
		fail();
	}
	
	@Test
	public void addActivityTest() throws DuplicateException, InvalidTimeException, IllegalNameException {
		Project p = new Project("proj");
		Activity a = new Activity("act");
		p.addActivity(a);
		assertTrue(p.containsActivity(a));
	}
	
	@Test
	public void addTwoActivitiesTest() throws DuplicateException, InvalidTimeException, IllegalNameException {
		Project p = new Project("proj");
		Activity a = new Activity("act");
		Activity a2 = new Activity("act2");
		p.addActivity(a);
		p.addActivity(a2);
		assertTrue(p.containsActivity(a2));
	}
	
	
}
