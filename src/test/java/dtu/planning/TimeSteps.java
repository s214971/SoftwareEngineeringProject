package dtu.planning;

import static org.mockito.Mockito.when;

import business.ProjectManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class TimeSteps {
	
	private TestContextHolder context;
	
	public TimeSteps(TestContextHolder context) {
		this.context = context;
	}
	
	@Given("the year is {int}")
	public void the_year_is(Integer int1) {
	    when(context.getTimeServer().getYear()).thenReturn(int1);
	}
	
	@When("the year changes to {int}")
	public void the_year_changes_to(Integer int1) {
	    when(context.getTimeServer().getYear()).thenReturn(int1);
	}
}
