package dtu.planning;
import static org.mockito.Mockito.mock;

import business.*;

public class TestContextHolder {
	
	private Project project;
	private Project foundProject;
	private Activity activity;
	private Developer developer;
	private Developer projectLeader;
	private String error;
	private TimeServer timeServer;
	
	public TestContextHolder(ProjectManager manager) {
		error = "";
		timeServer = mock(TimeServer.class);
		manager.setTimeServer(timeServer);
	}
	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	public Developer getProjectLeader() {
		return projectLeader;
	}
	
	public void setProjectLeader(Developer projectLeader) {
		this.projectLeader = projectLeader;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public Developer getDeveloper() {
		return developer;
	}

	public void setDeveloper(Developer developer) {
		this.developer = developer;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public TimeServer getTimeServer() {
		return timeServer;
	}

	public void setTimeServer(TimeServer timeServer) {
		this.timeServer = timeServer;
	}
	
	public Project getFoundProject() {
		return foundProject;
	}

	public void setFoundProject(Project foundProject) {
		this.foundProject = foundProject;
	}
}
