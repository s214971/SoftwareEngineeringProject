package dtu.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import business.Activity;
import business.DuplicateException;
import business.IllegalNameException;
import business.InvalidTimeException;
import business.NoElementException;
import business.Project;
import business.ProjectManager;
import io.cucumber.java.en.*;

public class ProjectSteps {

	private TestContextHolder context;
	private ProjectManager projectManager;
	
	public ProjectSteps(TestContextHolder context, ProjectManager projectManager) {
		this.context = context;
		this.projectManager = projectManager;
	}
	
	@Given("there is a project")
	public void there_is_a_project() throws IllegalNameException {
	    context.setProject(new Project("placeholder"));
	}
	
	@Given("there is a project with name {string}")
	public void there_is_a_project_with_name(String string) throws IllegalNameException {
		
		context.setProject(new Project(string));
	}

	@When("a user adds an activity to the project with name {string}")
	public void a_user_adds_an_activity_to_the_project(String activityName) throws InvalidTimeException, IllegalNameException {
		context.setActivity(new Activity(activityName));
	    try {
			context.getProject().addActivity(context.getActivity());
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}

	@Given("the project has an activity with the name {string}")
	public void the_project_has_an_activity_with_the_name(String activityName) {
		 try {
			Activity act = new Activity(activityName);
			context.getProject().addActivity(act);
			context.setActivity(act);
			
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}
	
	@Given("there is a project with an activity")
	public void there_is_a_project_with_an_activity() throws InvalidTimeException, IllegalNameException {
	    context.setProject(new Project("placeholder"));
	    context.setActivity(new Activity("placeholder"));
	    try {
			context.getProject().addActivity(context.getActivity());
		} catch (Exception e) {

			context.setError(e.getMessage());
		};
	}
	
	@Given("a project exists in the system with the name {string}")
	public void a_project_exists_in_the_system_with_the_name(String projectName) throws Exception {
	    try {
	    	Project p = new Project(projectName);
			projectManager.addProject(p);
			context.setProject(p);
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}

	@When("a project with the name {string} is added to the system")
	public void a_project_with_the_name_is_added_to_the_system(String projectName) throws Exception {
		try {
			
			projectManager.addProject(new Project(projectName));
			
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}
	
	@Given("no projects exist in the system")
	public void no_projects_exist_in_the_system() {
		assertTrue(projectManager.getProjects().size()==0);
	}

	@When("a user change the projects name to {string}")
	public void a_user_change_the_projects_name_to(String projectName) {
		 try {
			context.getProject().setName(projectName);
		} catch (IllegalNameException e) {
			context.setError(e.getMessage());
		}
	}

	@Then("the projects name is {string}")
	public void the_projects_name_is(String projectName) {
		assertEquals(context.getProject().getName(),projectName);
	}
	int projNum = 0;
	@When("{int} projects are added to the system")
	public void projects_are_added_to_the_system(Integer int1) {
	    for (int i = 0; i < int1; i++)
			try {
				projectManager.addProject(new Project("placeholder" + projNum++));
			} catch (Exception e) {
				context.setError(e.getMessage());
			}
	}
	
	@Then("a project with the id {string} is present in the system")
	public void a_project_with_the_id_is_present_in_the_system(String string) {
	    assertTrue(projectManager.containsProjectWithId(string));
	}

	@When("a user removes the project")
	public void a_user_removes_the_project(){
		try {
			projectManager.removeProject(context.getProject());
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}

	@When("the user tries to remove a project with name {string}")
	public void the_user_tries_to_remove_a_project_with_name(String projectName){
		try {
			projectManager.removeProject(projectManager.getProjectFromName(projectName));
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
		
	}

	@When("the user searches for a project with the name {string}")
	public void the_user_searches_for_a_project_with_the_name(String string) {
		try {
			context.setFoundProject(projectManager.getProjectFromName(string));
		}
		catch(Exception e) {
			context.setError(e.getMessage());
		}
	}
	@Then("the project is returned")
	public void the_project_is_returned() {
		
	    assertEquals(context.getProject(),context.getFoundProject());
	}
	
	@When("the user tries to remove the project")
	public void the_user_tries_to_remove_the_project() {
		try {
			projectManager.removeProject(context.getProject());
		} catch (NoElementException e) {
			context.setError(e.getMessage());
		}
	}
	
	@Then("the project is present in the system")
	public void the_project_is_present_in_the_system() {
	    projectManager.containsProject(context.getProject());
	}

	@Then("the project is on the archive list and not active")
	public void the_project_is_on_the_archive_list_and_not_active() {
	    projectManager.archiveContainsProject(context.getProject());
	}
}
