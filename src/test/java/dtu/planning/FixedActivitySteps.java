package dtu.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import business.Activity;

import business.Activity;
import business.DuplicateException;
import business.IllegalNameException;
import business.InvalidTimeException;
import business.NoElementException;
import business.PlanningAppException;
import business.Project;
import business.ProjectManager;
import business.Time;
import io.cucumber.java.en.*;

public class FixedActivitySteps {

	private TestContextHolder context;
	
	public FixedActivitySteps(TestContextHolder context) {
		this.context = context;
	}
	
	@When("the developer is added to the fixed activity {string} from week {int} to week {int}")
	public void the_developer_is_added_to_the_fixed_activity_from_week_to_week(String string, Integer int1, Integer int2) throws InvalidTimeException, IllegalNameException {
	    Activity act = new Activity(string);
	    context.setActivity(act);
	    try {
	    	act.setTimeFrame(int1, 0, int2, 0);
			context.getDeveloper().addFixedActivity(act);
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}
	
	@Then("the developer is registered to the fixed activity")
	public void the_developer_is_registered_to_the_fixed_activity() {
		assertTrue(context.getDeveloper().getFixedActivities().contains(context.getActivity()));
	}

	@Then("the developer is registered with {int} fixed activities")
	public void the_developer_is_registered_with_fixed_activities(Integer int1) {
	    assertTrue(int1 == context.getDeveloper().getFixedActivities().size());
	}

	@Given("the developer has the fixed activity {string}")
	public void the_developer_has_the_fixed_activity(String string) throws InvalidTimeException, DuplicateException, IllegalNameException {
		Activity act = new Activity(string);
	    act.setTimeFrame(1, 0, 2, 0);
	    context.setActivity(act);
	    context.getDeveloper().addFixedActivity(act);
	}

	@When("the developer is removed from the fixed activity")
	public void the_developer_is_removed_from_the_fixed_activity() {
		try {
			context.getDeveloper().removeFixedActivity(context.getActivity());			
		} catch(Exception ex) {
			context.setError(ex.getMessage());
		}
	}

	@Given("the developer has no fixed activities")
	public void the_developer_has_no_fixed_activities() {
		assertTrue(0 == context.getDeveloper().getFixedActivities().size());
	}
}
