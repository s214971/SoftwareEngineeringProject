package dtu.planning;

import static org.junit.Assert.*;

import io.cucumber.java.en.*;

public class GeneralSteps {

	private TestContextHolder context;
	
	public GeneralSteps(TestContextHolder context) {
		this.context = context;
	}

	@Then("an error with the message {string} is thrown")
	public void an_error_with_the_message_is_thrown(String string) {
	    assertEquals(string, context.getError());
	}
}
