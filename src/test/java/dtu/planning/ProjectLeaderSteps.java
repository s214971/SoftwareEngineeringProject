package dtu.planning;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import business.Developer;
import business.IllegalNameException;
import business.Project;
import business.ProjectLeaderException;
import business.ProjectManager;
import io.cucumber.java.en.*;

public class ProjectLeaderSteps {
	
	private TestContextHolder context;
	private ProjectManager projectManager;
	private Developer developer;
	
	public ProjectLeaderSteps(TestContextHolder context, ProjectManager projectManager) {
		this.context = context;
		this.projectManager = projectManager;
		
	}

	@Given("there hasn't been added a project leader to the project")
	public void there_hasn_t_been_added_a_project_leader_to_the_project() {
		assert(context.getProjectLeader() == null);
	}

	@When("the developer is added as a project leader to the project")
	public void the_developer_is_added_as_a_project_leader_to_the_project() {
	    try {
			context.getProject().addProjectLeader(context.getDeveloper());
		} catch (ProjectLeaderException e) {
			context.setError(e.getMessage());
		}
	}

	@Then("the developer is the project's project leader")
	public void the_developer_is_the_project_s_project_leader() {
		assertEquals(context.getDeveloper(), context.getProject().getProjectLeader());
	}

	@Given("the project already has a another developer assigned as the project leader")
	public void the_project_already_has_a_another_developer_assigned_as_the_project_leader() throws IllegalNameException, ProjectLeaderException {
		developer = new Developer("init","name");
		context.getProject().addProjectLeader(developer);
	
	}
	
	@Given("the developer is assigned as the project's leader")
	public void the_developer_is_assigned_as_the_project_s_leader() {
	    try {
				context.getProject().addProjectLeader(context.getDeveloper());
			} catch (ProjectLeaderException e) {
				context.setError(e.getMessage());
			}
		}

	@When("the project leader is removed from the project")
	public void the_project_leader_is_removed_from_the_project() throws ProjectLeaderException {
	    try {
			context.getProject().removeProjectLeader();
		} catch (ProjectLeaderException e) {
			context.setError(e.getMessage());
		}
	}

	@Then("the project doesn't have a project leader")
	public void the_project_doesn_t_have_a_project_leader() {
		assertEquals(context.getProject().getProjectLeader(), null);
	}
}
