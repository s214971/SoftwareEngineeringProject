package dtu.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import business.Activity;

import business.Activity;
import business.DuplicateException;
import business.IllegalNameException;
import business.InvalidTimeException;
import business.NoElementException;
import business.PlanningAppException;
import business.Project;
import business.ProjectManager;
import business.Time;
import io.cucumber.java.en.*;

public class ActivitySteps {

	private TestContextHolder context;
	private ProjectManager projectManager;

	public ActivitySteps(TestContextHolder context, ProjectManager projectManager) {
		this.context = context;
		this.projectManager = projectManager;
	}

	@When("the user sets the start time to week {int} year {int} and the end time to week {int} year {int}")
	public void the_user_sets_the_start_time_to_week_year_and_the_end_time_to_week_year(Integer int1, Integer int2, Integer int3, Integer int4) {
		try {
			context.getActivity().setTimeFrame(int1,int2,int3,int4);
		} catch (InvalidTimeException e) {
			context.setError(e.getMessage());
		}
	}

	@Then("the activitys start time will be week {int} and year {int}")
	public void the_activitys_start_time_will_be_week_and_year(Integer int1, Integer int2) {
	    assertEquals(context.getActivity().getStartDate().getWeek(),(int)int1);
	    assertEquals(context.getActivity().getStartDate().getYear(),(int)int2);

	}

	@Then("the activitys end time will be week {int} and year {int}")
	public void the_activitys_end_time_will_be_week_and_year(Integer int1, Integer int2) {
		assertEquals(context.getActivity().getEndDate().getWeek(),(int)int1);
	    assertEquals(context.getActivity().getEndDate().getYear(),(int)int2);
	}

	@When("the developer is added to the activity")
	public void the_developer_is_added_to_the_activity() {
	    try {
			context.getActivity().addDeveloper(context.getDeveloper(), projectManager);
		} catch (PlanningAppException e) {
			context.setError(e.getMessage());
		}
	}

	@Given("the developer is assigned to the activity")
	public void the_developer_is_assigned_to_the_activity() {
		try {
			context.getActivity().addDeveloper(context.getDeveloper(), projectManager);
		} catch (PlanningAppException e) {
			context.setError(e.getMessage());
		}
	}

	@Given("there is an activity with name {string}")
	public void there_is_an_activity_with_name(String activityName) throws InvalidTimeException, IllegalNameException {
		 context.setActivity(new Activity(activityName));
	}

	@When("a user change the activity name to {string}")
	public void a_user_change_the_activity_name_to(String activityName) throws IllegalNameException {
		context.getActivity().setName(activityName);
	}

	@Then("the activity name is {string}")
	public void the_activity_name_is(String activityName) {
		assertEquals(context.getActivity().getName(),activityName);
	}

	@When("the activitys expected time is set to {int} hours")
	public void the_activitys_expected_time_is_set_to_hours(Integer hours) {
		try {
			context.getActivity().setExpectedTime(new Time(hours, 0, 0));
		} catch (PlanningAppException e) {
			context.setError(e.getMessage());
		}
	}

	@Then("the activitys expected time is {int} hours")
	public void the_activitys_expected_time_is_hours(Integer hours) {
	    assertEquals(hours.intValue(), context.getActivity().getExpectedTime().getHours());
	}

	@Given("the developer is on {int} activities")
	public void the_developer_is_on_activities(Integer int1) throws Exception {
		Project proj = new Project("the_dev_is_on_x_activities_project_placeholder");
		projectManager.addProject(proj);
		for (int i = 0; i < int1; i++) {
			Activity act = new Activity("the_dev_is_on_x_activities_activity_placeholder_" + i);
			proj.addActivity(act);
			act.addDeveloper(context.getDeveloper(), projectManager);
		}
	}

	@Then("the activity has {int} developers on it")
	public void the_activity_has_developers_on_it(Integer int1) {
	    assertEquals(int1.intValue(), context.getActivity().getDevCount());
	}

	@Given("the project has an activity")
	public void the_project_has_an_activity() throws Exception {
		Activity a = new Activity("placeholder");
		context.setActivity(a);
	    context.getProject().addActivity(a);
	}

	@When("a user removes the activity")
	public void a_user_removes_the_activity() {
		try {
			context.getProject().removeActivity(context.getActivity());
		} catch (NoElementException e) {
			context.setError(e.getMessage());
		}
	}

	@Then("the project doesnt contain the activity")
	public void the_project_doesnt_contain_the_activity() {
	    assertFalse(context.getProject().containsActivity(context.getActivity()));
	}

	@Then("the projects removed activities list contains the activity")
	public void the_projects_removed_activities_list_contains_the_activity() {
	    assertTrue(context.getProject().getRemovedActivities().contains(context.getActivity()));
	}

	@Then("the project has contains the activity")
	public void the_project_has_contains_the_activity() {
		assertTrue(context.getProject().getActivities().contains(context.getActivity()));
	}

	@Given("there is an activity")
	public void there_is_an_activity() throws InvalidTimeException, IllegalNameException {
	   context.setActivity(new Activity("Dark Theme"));
	}

	@When("the user registers the spent time of the developer on the activity as {int} hours and {int} minutes")
	public void the_user_registers_the_spent_time_of_the_developer_on_the_activity_as_hours_and_minutes(int hours, int minutes) {
	    try {
			context.getActivity().registerDeveloperHours(context.getDeveloper(), new Time(hours, minutes));
		} catch (InvalidTimeException e) {
			context.setError(e.getMessage());
		}
	}

	@Then("the registered time for the developer on the activity will be {int} hours and {int} minutes")
	public void the_registered_time_for_the_developer_on_the_activity_will_be_hours_and_minutes(int hours, int minutes) throws InvalidTimeException {
		Time t1;
		t1 = context.getActivity().getRegisteredDeveloperHours(context.getDeveloper());
		Time t2;
		try {
			t2 = new Time(hours, minutes);
			assertEquals(t1.getMillis(),t2.getMillis());
		} catch (InvalidTimeException e) {
			context.setError(e.getMessage());
		}
	}

	@When("the user resets the developers hours on the activity")
	public void the_user_resets_the_developers_hours_on_the_activity() {
	    context.getActivity().resetTime(context.getDeveloper());
	}

	@Then("the residue hours for the activity is {int} hours")
	public void the_residue_hours_for_the_activity_is_hours(int int1) {
	   try {
		assertEquals(Integer.toString(int1), context.getActivity().getLeftoverExpectedHours());
	} catch (InvalidTimeException e) {
		context.setError(e.getMessage());
	}
	}

	@Then("the residue hours for the activity is {string}")
	public void the_residue_hours_for_the_activity_is(String string) {
		try {
			assertEquals(string, context.getActivity().getLeftoverExpectedHours());
		} catch (InvalidTimeException e) {
			context.setError(e.getMessage());
		}
	}
}
