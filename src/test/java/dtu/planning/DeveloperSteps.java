package dtu.planning;

import static org.junit.Assert.*;

import business.*;
import io.cucumber.java.en.*;

public class DeveloperSteps {

	private TestContextHolder context;
	private ProjectManager projectManager;

	public DeveloperSteps(TestContextHolder context, ProjectManager projectManager) {
		this.context = context;
		this.projectManager = projectManager;
	}

	@Given("there is a developer in the system")
	public void there_is_a_developer_in_the_system() {
	    try {
	    	Developer developer = new Developer("init","name");
		    context.setDeveloper(developer);
	    	projectManager.addDeveloper(developer);
			
		} catch (PlanningAppException e) {
			context.setError(e.getMessage());
		} 
	    
	}
	
	@Given("there is a developer")
	public void there_is_a_developer() {
		try {
			Developer developer = new Developer("init","name");
			context.setDeveloper(developer);
		} catch (PlanningAppException e) {
			context.setError(e.getMessage());
		} 
	}
	
	@Given("the developer is not in the system")
	public void the_developer_is_not_in_the_system() {
		assertFalse(projectManager.containsDeveloper(context.getDeveloper()));
	}
	
	@When("the developer is added to the system")
	public void the_developer_is_added_to_the_system() {
	    try {
			projectManager.addDeveloper(context.getDeveloper());
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}
	
	
	@Given("the developer has been added the system")
	public void the_developer_has_been_added_the_system() {
	    try {
			projectManager.addDeveloper(context.getDeveloper());
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}

	
	@Then("the developer is in the system")
	public void the_developer_is_in_the_system() {
		assertTrue(projectManager.containsDeveloper(context.getDeveloper()));
	}
	
	

	@Given("there is a developer with Id {string} and name {string}")
	public void there_is_a_developer_with_id_and_name(String initials, String name) {
		try {
			Developer developer = new Developer(initials,name);
			context.setDeveloper(developer);
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}
	
	@When("a developer with Id {string} and name {string} is created")
	public void a_developer_with_id_and_name_is_created(String initials, String name) {
		try {
			Developer developer = new Developer(initials, name);
			context.setDeveloper(developer);
		} catch (Exception e) {
			context.setError(e.getMessage());
		}
	}
}
