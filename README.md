This is a Java project made as part of the Software Engineering 1 course at DTU

This project was made by:
Andreas Verdoner Barba,
Helena Tejs Knudsen,
Mia Fenja Kringelhede,
and Nikolaj Just Hjortshøj

The repository URL is https://gitlab.gbar.dtu.dk/s214971/SoftwareEngineeringProject

################################################################################

The project is about using class oriented programming, common design patterns
and SOLID-principles, to design and implement Software for a hypothetical
company.
In this project, we're creating software for a company to track it's projects,
activities and how much time the company's developers have spent on activities.

################################################################################
How to use the software.

To run the program:
  1. Import the project to Eclipse, or the IDE of your preference
  2. Go to: src/main/java/presentation/
  3. Run the file Program.java
Alternatively:
  1. Run java.exe using target/classes as the class path and running target/classes/presentation/Program as the entry point.
The program is text-based. By writing 'help' in the console, you can see a list of commands.

To run the tests:
  1. Import the project to Eclipse, or the IDE of your preference
  2. Run the folder: src/test/java as JUnit test (Alternatively, you can run the entire project as a JUnit test)

################################################################################
The project was built on a template.

From template:
It contains all the necessary libraries to run Cucumber tests, JUnit 5 tests, and JUnit 4 tests. In addition, it contains the reference to the Mockito libraries.
It can be run through Maven, e.g., `mvn clean test`, Eclipse (run as JUnit test), and ItelliJ.
################################################################################
