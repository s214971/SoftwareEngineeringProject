Feature: Add and remove developers to/from fixed activities
   Description: Add and remove developers to/from fixed activities. 
   The activities are personal to the developer and are not shared.
   Actor: User

# Scenario: Add user to fixed activity successfully
#     Given a developer is registered in the system
#     When the developer is added to the fixed activity "Holiday" from week 6 to week 7
#     Then the developer is registered to a fixed activity called "Holiday" from week 6 to week 7
#
# Scenario: Add developer to fixed activity with starting time later than ending time
#     Given a developer is registered in the system
#     When the developer is added to the fixed activity "Holiday" from week 8 to week 5
#     Then an error with the message "Starting time has to be before the ending time" is thrown
#     And the developer is registered with 0 fixed activities
#
# Scenario: Add developer to a fixed activity with the same identifier as an existing one
#     Given a developer is registered in the system
#     When the developer is added to the fixed activity "Sick" from week 18 to week 19
#     And the developer is added to the fixed activity "Sick" from week 18 to week 19
#     Then an error with the message "Fixed activity already exists for user" is thrown
#     And the developer is registered with 1 fixed activities
    #
# Scenario: Remove a developer from a fixed activity successfully
#     Given a developer is registered in the system
#     And the developer has the fixed activity "Course"
#     When the developer is removed from the fixed activity "Course"
#     Then the developer is registered with 0 fixed activities
    #
# Scenario: Remove a developer from a non-existent fixed activity
#     Given a developer is registered in the system
#     And the developer has no fixed activities
#     When the developer is removed from the fixed activity "Non-existent"
#     Then an error with the message "Fixed activity doesn't exist" is thrown
    