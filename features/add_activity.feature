#Author Helena
 Feature: Add activity to project
   Description: A user can add activities to projects
   Actors: user
 
 Scenario: User successfully adds an activity to a project
    Given there is a project
    When a user adds an activity to the project with name "Startup"
    Then the project has contains the activity
    
 Scenario: User tries to add an activity with a duplicate name
    Given there is a project with name "P1234"
    And the project has an activity with the name "Bugfixing"
    When a user adds an activity to the project with name "Bugfixing"
    Then an error with the message "Duplicate activity in project P1234" is thrown