#Author Helena
 Feature: Remove activity from project
   Description: A user can remove activities from projects
   Actors: user
 
 Scenario: User successfully removes an activity from a project
    Given there is a project
    And the project has an activity
    When a user removes the activity
    Then the project doesnt contain the activity
    And the projects removed activities list contains the activity
    
 Scenario: User tries to remove an activity that does not exist
    Given there is a project
    When a user removes the activity
    Then an error with the message "Activity does not exist" is thrown


    