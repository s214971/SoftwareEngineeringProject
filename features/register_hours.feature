#Author Nikolaj
Feature: Register hours
  A user should be able to register how much time a developer has spent on an activity

  Scenario: A user succesfully registers time on an activity
    Given there is an activity
    And there is a developer
    When the user registers the spent time of the developer on the activity as 2 hours and 30 minutes
    Then the registered time for the developer on the activity will be 2 hours and 30 minutes

  Scenario: A user tries to register negative hours
    Given there is an activity
    And there is a developer
    When the user registers the spent time of the developer on the activity as -2 hours and 30 minutes
    Then an error with the message "Invalid time." is thrown
    
   Scenario: A developer successfully registers additional time on an activity
   Given there is an activity
   And there is a developer
   And the user registers the spent time of the developer on the activity as 2 hours and 30 minutes
   When the user registers the spent time of the developer on the activity as 2 hours and 30 minutes
   Then the registered time for the developer on the activity will be 5 hours and 0 minutes
   
	Scenario: A user resets a developers registered hours
		Given there is an activity
		And there is a developer
		And the user registers the spent time of the developer on the activity as 2 hours and 30 minutes
		When the user resets the developers hours on the activity
		Then the registered time for the developer on the activity will be 0 hours and 0 minutes
	
	Scenario: A user resets a developers registered hours, who has not registered any hours
		Given there is an activity
		And there is a developer
		When the user resets the developers hours on the activity
		Then the registered time for the developer on the activity will be 0 hours and 0 minutes
 