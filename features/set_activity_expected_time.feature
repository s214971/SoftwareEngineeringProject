#Author Nikolaj

Feature: Set an expected time to an activity
 	Actor: User

  Scenario: Set an expected time to an activity successfully
 		Given there is a project with an activity
 		When the activitys expected time is set to 5 hours
 		Then the activitys expected time is 5 hours
 		
  Scenario: Set a negative expected time to an activity
 		Given there is a project with an activity
 		When the activitys expected time is set to -3 hours
 		Then an error with the message "Invalid time." is thrown
 	