#Author Andreas
Feature: Add developer to activity
  Actor: User
  
  
  Scenario: Developer is sucessfully added to activity
  	Given there is a developer in the system
  	And there is a project with an activity
  	When the developer is added to the activity
  	Then the developer is assigned to the activity
  	And the activity has 1 developers on it
  	
  Scenario: Developer is already assigned to the activity
  	Given there is a developer in the system
  	And there is a project with an activity
  	And the developer is assigned to the activity
  	When the developer is added to the activity
  	Then an error with the message "Developer is already added to the activity" is thrown
  	And the activity has 1 developers on it
 
  Scenario: Developer is sucessfully added to activity
  	Given there is a developer in the system
  	And the developer is on 20 activities
  	And there is a project with an activity
  	When the developer is added to the activity
  	Then an error with the message "Developer has reached the maximum amount of activities" is thrown
  	And the activity has 0 developers on it
