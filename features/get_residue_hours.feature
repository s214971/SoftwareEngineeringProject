#Author Nikolaj
#Scenario - After expected time is assigned to a project
#A user can ask how much time is left, which will decrease when developers register time.

Feature: Get Residue Hours



Scenario: A user gets residue hours for an activity with assigned time, and no registered hours
  Given there is a project with an activity
 	When the activitys expected time is set to 5 hours
 	Then the residue hours for the activity is 5 hours
 	
Scenario: A user gets residue hours for an activity with assigned time and registered hours
	Given there is a project with an activity
	And there is a developer
 	When the activitys expected time is set to 5 hours
  And the user registers the spent time of the developer on the activity as 2 hours and 0 minutes
 	Then the residue hours for the activity is 3 hours

Scenario: A user gets residue hours for an activity with no assigned time
	Given there is a project with an activity
	Then the residue hours for the activity is "0"

