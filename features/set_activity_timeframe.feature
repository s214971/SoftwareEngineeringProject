#Author Nikolaj
Feature: Set timeframe for activity
  Actor: User


 Scenario: User successfully adds start and end date to activity
    Given there is an activity with name "Startup"
    When the user sets the start time to week 50 year 2021 and the end time to week 51 year 2021
    Then the activitys start time will be week 50 and year 2021
    And the activitys end time will be week 51 and year 2021
        
Scenario: User tries to set end time before start time
    Given there is an activity with name "Startup"
    When the user sets the start time to week 51 year 2021 and the end time to week 50 year 2021
    Then an error with the message "The end time cannot be before the start time" is thrown
 
Scenario: User tries to set negative start week
    Given there is an activity with name "Startup"
    When the user sets the start time to week -51 year 2021 and the end time to week 50 year 2021
    Then an error with the message "The week number must be a positive integer" is thrown

Scenario: User tries to set negative end week
    Given there is an activity with name "Startup"
    When the user sets the start time to week 51 year 2021 and the end time to week -50 year 2021
    Then an error with the message "The week number must be a positive integer" is thrown
              
Scenario: User tries to set negative start year
    Given there is an activity with name "Startup"
    When the user sets the start time to week 51 year -2021 and the end time to week 50 year 2021
    Then an error with the message "The year must be a positive integer" is thrown
              
Scenario: User tries to set negative end year
    Given there is an activity with name "Startup"
    When the user sets the start time to week 51 year 2021 and the end time to week 50 year -2021
    Then an error with the message "The year must be a positive integer" is thrown
  
Scenario: User tries to set startweek larger than no of week numbers
    Given there is an activity with name "Startup"
    When the user sets the start time to week 58 year 2021 and the end time to week 59 year 2021
    Then an error with the message "Week numbers may be no larger than 53" is thrown
   
Scenario: User tries to set endweek larger than no of week numbers
    Given there is an activity with name "Startup"
    When the user sets the start time to week 51 year 2021 and the end time to week 59 year 2021
    Then an error with the message "Week numbers may be no larger than 53" is thrown
                     

