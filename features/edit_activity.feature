#Author Mia
Feature: Edit activity
	Description: A user can edit activities
	Actor: User
	
Scenario: User successfully change activity name
	Given there is an activity with name "Activity 1"
	When a user change the activity name to "A1"
	Then the activity name is "A1"
