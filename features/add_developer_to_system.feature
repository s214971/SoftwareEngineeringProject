

Feature: Add developer to system
 	Actor: User
#Author Mia

  Scenario: Add a developer sucessfully to the system
 		Given there is a developer
 		And the developer is not in the system
 		When the developer is added to the system
 		Then the developer is in the system
 	
 	Scenario: Added developer has id which is already in the system
 		Given there is a developer with Id "jona" and name "Jonathan"
 		And the developer has been added the system
 		And there is a developer with Id "jona" and name "Jonas"
 		When the developer is added to the system
 		Then an error with the message "There is already a developer with ID: jona in system" is thrown
 		
 	Scenario: Added developer whose id is too long
 		When a developer with Id "jonathan" and name "Jonathan Smith" is created
 		Then an error with the message "Developer ID has to be letters and no more than 4 characters" is thrown
 		
	Scenario: Added developer whose id is too long
 		When a developer with Id "jona1234" and name "Jonathan Smith" is created
 		Then an error with the message "Developer ID has to be letters and no more than 4 characters" is thrown
 	