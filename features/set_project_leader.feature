#Author Helena
Feature: Add/remove project leader to project
   Description: Add a project leader to a project or remove a project leader from a project
   Actors: User
    
Scenario: Project leader is successfully added to a project
    Given there is a project
    And there is a developer
    And there hasn't been added a project leader to the project
    When the developer is added as a project leader to the project
    Then the developer is the project's project leader

Scenario: Try to add a project leader to a project that already has a project leader
    Given there is a project
    And there is a developer
    And the project already has a another developer assigned as the project leader
    When the developer is added as a project leader to the project
    Then an error with the message "Project has already been assigned a leader" is thrown

Scenario: Project leader is successfully removed from a project
    Given there is a project
    And there is a developer
    And the developer is assigned as the project's leader
    When the project leader is removed from the project
    Then the project doesn't have a project leader

Scenario: Project leader is not added but tries to be removed
    Given there is a project
    And the project doesn't have a project leader
    When the project leader is removed from the project
    Then an error with the message "Project does not have a leader" is thrown