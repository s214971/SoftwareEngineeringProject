#Author Mia
Feature: Edit project
	Description: A user can edit projects
	Actor: User
	
Scenario: User successfully change project name
	Given there is a project with name "Proj1"
	When a user change the projects name to "P1"
	Then the projects name is "P1"