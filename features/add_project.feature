#Author Andreas 
Feature: Create a project
  Description: Add projects to the system.
  Actor: User

Scenario: Create a project successfully
   Given no projects exist in the system
   When a project with the name "Test project" is added to the system
   Then the project is present in the system
   
Scenario: Create a project with an empty name
   When a project with the name "" is added to the system
   Then an error with the message "Project needs a name" is thrown

Scenario: Create projects with correct Ids
   Given the year is 2022
   And no projects exist in the system
   When 1 projects are added to the system
   Then a project with the id "220001" is present in the system
   When 1 projects are added to the system
   Then a project with the id "220002" is present in the system
   When the year changes to 2023
   And 1 projects are added to the system
   Then a project with the id "230001" is present in the system
   
Scenario: Sucessfully get project from id:
   Given a project exists in the system with the name "Boogie Woogie"
   When the user searches for a project with the name "Boogie Woogie"
   Then the project is returned

Scenario: Fails to get project from id:
   Given a project exists in the system with the name "Boogie Woogie"
   When the user searches for a project with the name "Boogie Woo"
   Then an error with the message "No project with name: Boogie Woo" is thrown
   
