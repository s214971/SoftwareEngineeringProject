#Author Mia

Feature: Remove project
	Description: A user can remove a project
	Actor: User
	
Scenario: User successfully remove project
  Given a project exists in the system with the name "Project 1"
	When a user removes the project
	Then the project is on the archive list and not active
	
Scenario: User removes project that does not exist
	Given there is a project with name "Project 1"
	When the user tries to remove a project with name "Project 2"
	Then an error with the message "No project with name: Project 2" is thrown
	
Scenario: User removes project that does not exist
	Given there is a project
	When the user tries to remove the project
	Then an error with the message "Project doesn't exist in the system" is thrown